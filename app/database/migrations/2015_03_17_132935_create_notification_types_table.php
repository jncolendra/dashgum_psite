<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('notification_types', function($table)
		    {
		        $table->increments('id');	//Event ID
		        $table->string('type');	//Event Place
		        $table->string('message');
		    });
		DB::table('notification_types')->insert(
			array(
				"type" => "CreatedEvent",
				"message" => "PSITE(admins) invites you to the event :",
			)
		);
		DB::table('notification_types')->insert(
			array(
				"type" => "RegisteredUser",
				"message" => "has registered.",
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('notification_types');
	}

}
