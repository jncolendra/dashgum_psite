<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenewalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Renewal Table for Accounts
		Schema::create('renewals', function($table)
		    {
		        $table->increments('id');	
		        $table->string('user_id');
		        $table->date('renewal_date');	
		        $table->string('is_paid');
		        $table->double('payment_amount');
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('renewals');
	}

}
