<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function($table)
		    {
		        $table->increments('id');	//Event ID
		        $table->string('event_title');	//Event Title
		        $table->date('event_date');		//Event Date
		        $table->string('event_place');	//Event Place
		        $table->string('event_photo');	//Event Photo File Name
		        $table->string('event_description');	//Description
		        $table->string('event_creator_id'); 	//Event Creator of ID
		        $table->timestamps();
		        $table->softDeletes();
		    });

		/*Seed Sample Event*/
		DB::table('events')->insert(array(
			"event_title"	=> "Sample Event",
			"event_date"	=> date("Y-m-d"),
			"event_place"	=> "Sample Place",
			"event_photo"	=> "psitencr.jpg",
			"event_description"	=> "Sample Description",
			"event_creator_id"	=> 1,
			"created_at"	=>	date("Y-m-d")
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
