<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('notifications', function($table)
		    {
		        $table->increments('id');	
		        $table->string('sender_id');	
		        $table->string('receiver_id');		
		        $table->string('type_id');	
		        $table->string('extra');
		        $table->dateTime('created_at');	
		        $table->string('url_id'); 
		        $table->string('state');
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('notifications');
	}

}
