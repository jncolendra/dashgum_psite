<?php

use Illuminate\Database\Migrations\Migration;

class ConfideSetupUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Creates the users table
        Schema::create('users', function ($table) {
            $table->increments('id');
            $table->string('membership_id')->unique();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('approval'); // Pending, Approved
            $table->string('usertype'); //Indiv, Insti, Corpo
            $table->integer('is_admin'); // Admin, Superuser
            $table->integer('is_online'); // Flags for Online or Not
            $table->string('officer_position'); // Is officer at PSITE?
            $table->string('photofilename');
            $table->string('payment_photo'); //Deposit Slip
            $table->double('payment');  //Payment in Ammount
            $table->string('password');

            $table->string('individual_fname')->nullable();         //Individual Start
            $table->string('individual_lname')->nullable();         //Database Entry
            $table->string('individual_affiliation')->nullable();   //Database Entry
            $table->string('individual_president')->nullable();     //Database Entry
            $table->string('individual_attainment')->nullable();    //Individual Stop

            $table->string('institution_affiliation')->nullable();  //Institutional Start
            $table->string('institution_address')->nullable();      //Database Entry
            $table->string('institution_rep1_name')->nullable();    //Database Entry
            $table->string('institution_rep1_contact')->nullable(); //Database Entry
            $table->string('institution_rep2_name')->nullable();    //Database Entry
            $table->string('institution_rep2_contact')->nullable(); //Database Entry
            $table->string('institution_president')->nullable();    //Instituional Stop

            $table->string('corporate_companyname')->nullable();    //Corporate Start
            $table->string('corporate_address')->nullable();        //Database Entry
            $table->string('corporate_rep')->nullable();            //Database Entry
            $table->string('corporate_rep_contact')->nullable();    //Corporate Stop

            $table->date('first_renewal_date')->nullable();         //Date of Renewal
            $table->string('confirmation_code');
            $table->string('remember_token')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        // Creates password reminders table
        Schema::create('password_reminders', function ($table) {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });
        //Seeding Code
        $user = new User;
        $user->membership_id = date("Y")."-0001";
        $user->username = 'johndoe';
        $user->email = 'johndoe@site.dev';
        $user->password = 'foo_bar_1234';
        $user->password_confirmation = 'foo_bar_1234';
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->first_renewal_date = date("Y-m-d");
        $user->confirmed = 1;
        $user->is_admin = 1;
        $user->photofilename = "johndoe.gif";
        if(! $user->save()) {
            Log::info('Unable to create user '.$user->email, (array)$user->errors());
        } else {
            Log::info('Created user '.$user->email);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('password_reminders');
        Schema::drop('users');
    }
}
