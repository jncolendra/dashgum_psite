<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//Views
Route::get('/', function(){ // Langind Page
	return View::make('login');
});
Route::get('/archiveEvents',function(){
	return View::make('admin.archiveEvents');
});
Route::get('/archive', function(){
	return View::make('admin.archive');
});
Route::get('/ActiveMembers', function(){
	return View::make('admin.activeMembersView');
});
Route::get('/deleteInactive/{id}', "MainController@deleteInactive");
Route::get('/faq', function(){
	return View::make("app.faq");
});
Route::get('/removeSession', function(){
	if(Request::ajax()){
	Session::forget("notice");
	}
});
Route::get('/payment',function(){
	return View::make('payment');
});
Route::get('/payReg', function(){
	return View::make('admin.accountingReg');
});
Route::get('/payRen', function(){
	return View::make('admin.accountingRen');
});
Route::get('/membersreport',function(){
	return View::make('admin.member_reports');
});
Route::post('/updateEvent', "MainController@updateEvent");
Route::get('/issuecerts', function(){
	return View::make('admin.issuecerts');
});
Route::get('/certview/{id}', function($id){
	return View::make('admin.certificate')->with('id',$id);
});
Route::get('/eventListAttendance', function(){
	return View::make('admin.listEventsAttendance');
});
Route::post('/settlepayment',"MainController@settlePayment");
Route::post('/submitPayment',"MainController@submitPayment");
/*Admin Views - START*/
Route::get('/admin',array('before' => 'admin', function(){ //Control Panel for Admin
	$users = User::where('approval','pending')->get(); //Model Call
	return View::make('admin.controlpanel')->with('users',$users);
}));
Route::get('/registeredusers',array('before' => 'admin', function(){
	$users = User::all(); //Model Call
	return View::make('admin.registeredusers')->with('users',$users);
}));
Route::get('/createannouncement',array('before' => 'admin', function(){
	return View::make('admin.createannouncement');
}));
Route::get('/approvalofmembers',array('before' => 'admin',function(){
	$users = User::where('approval',"pending")->get();
	return View::make('admin.approvalofmembers')->with('users',$users);
}));
Route::get('/accounting',function(){
	return View::make('admin.accounting');
});
/*Approve User*/
Route::get('/approveuser/{id}', "MainController@approveUser");

/*List of Members Individual*/
Route::get('/listofmembers_individual', "MainController@listofMembers_individual");

/*List of Members Instituional*/
Route::get('/listofmembers_institutional', "MainController@listofMembers_institutional");

/*List of Members Corporate*/
Route::get('/listofmembers_corporate', "MainController@listofMembers_corporate");

/*Uploads Profile Photo of Admin*/
Route::post('/uploadPhoto',"MainController@profilePhoto");
/* Create Event - In Progress*/
Route::post('/createEvent', "MainController@createEvent");

Route::post('/submitAttendance',"MainController@submitAttendance");

/*List of Events*/
Route::get('/listofevents', "MainController@listofEvents");
/*Delete Event*/
Route::get('/deleteevent/{id}', "MainController@deleteEvent");

Route::get('/eventAttendance',function(){
	return View::make('admin.eventAttendance');
});
/*Get Online Users*/
Route::get('/getonline',"MainController@getOnline");
/*@STOP*/

/*About Us*/
Route::get('/aboutus', function(){
	return View::make('app.aboutus');
	});
/*Membership Benefits*/
Route::get('/membershipbenefits', function(){

	return View::make('app.membershipbenefits');
});

/*Common Views*/
Route::get('/profile/{username}', "MainController@getProfileData");

Route::post('/searchBox', "MainController@searchBar");
Route::get('/searchResults', function(){
	return View::make("app.searchresults");
});
/*User Views*/
Route::get('/dashboard',array('before' => 'approval',function(){ //Dashboard for Users
	return View::make('app.feed');
}));
Route::get('/events/{id}', "MainController@viewEvents");
Route::get('/deleteAttendance/{id}/{id2}', "MainController@deleteAttendance");
Route::get('/sendMessage/{receiverId}', "MainController@createConvo");
Route::get('/inbox', "MainController@viewInbox");


Route::post('/deliverMessage',"MainController@sendMessage");
/*Get Username - Ajax*/
Route::post('/getUsername', "MainController@getUsername");
Route::post('/search', function(){
	$x = Input::get("searchInput");
	return $x;
});
Route::post('/submitRenewal', "MainController@submitRenewal");
Route::get('/certAttendance/{id}/{id2}', function($id, $id2){
	return View::make("admin.certificateattendance")->with('id',$id)->with('id2',$id2);
});
// Confide routes
Route::get('users/create', 'UsersController@create');//Sign Up
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login'); //Login
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
Route::get('/readNotifs/{id}/{id2}/{id3}', 'MainController@readNotifs');
Route::get('test',function(){
});
/*Ajax Awesomeness Below*/
Route::get('/idle', "MainController@ajaxIdle");
Route::get('/active', "MainController@ajaxActive");
Route::get('/msgcount', "MainController@getMsgCount");
Route::get('/quickbox', "MainController@realtimeQuickInbox");
Route::get('/getnotif', "MainController@realtimeNotif");
Route::get('/notifcount', "MainController@getNotifCount");
Route::get('/rltinbox', "MainController@realtimeInbox");