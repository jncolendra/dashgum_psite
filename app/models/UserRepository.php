<?php



/**
 * Class UserRepository
 *
 * This service abstracts some interactions that occurs between Confide and
 * the Database.
 */
class UserRepository
{
    /**
     * Signup a new account with the given parameters
     *
     * @param  array $input Array containing 'email' and 'password'.
     *
     * @return  User User object that may or may not be saved successfully. Check the id to make sure.
     */
    public function signup($input)
    {
        $user = new User;
        $user->username = array_get($input, 'username');
        $user->email    = array_get($input, 'email');
        $user->password = array_get($input, 'password');
        $user->usertype = array_get($input, 'usertype'); //Gets User Type
        $user->officer_position = array_get($input, 'officer_position'); // Stores Officer Position
        $user->photofilename = "johndoe.gif"; // DefaultProfilePicture
        $user->approval = "pending"; //always pending at first change at API

        $user->individual_fname         = array_get($input, 'individual_fname');
        $user->individual_lname         = array_get($input, 'individual_lname');
        $user->individual_affiliation   = array_get($input, 'individual_affiliation');
        $user->individual_president     = array_get($input, 'individual_president');
        $user->individual_attainment    = array_get($input, 'individual_attainment');

        $user->institution_affiliation  = array_get($input, 'institution_affiliation');
        $user->institution_address      = array_get($input, 'institution_address');
        $user->institution_rep1_name    = array_get($input, 'institution_rep1_name');
        $user->institution_rep1_contact = array_get($input, 'institution_rep1_contact');
        $user->institution_rep2_name    = array_get($input, 'institution_rep2_name');
        $user->institution_rep2_contact = array_get($input, 'institution_rep2_contact');
        $user->institution_president    = array_get($input, 'institution_president');

        $user->corporate_companyname    = array_get($input, 'corporate_companyname');
        $user->corporate_address        = array_get($input, 'corporate_address');
        $user->corporate_rep            = array_get($input, 'corporate_rep');
        $user->corporate_rep_contact    = array_get($input, 'corporate_rep_contact');
        $user->first_renewal_date       = date("Y-m-d");
        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $user->password_confirmation = array_get($input, 'password_confirmation');

        // Generate a random confirmation code
        $user->confirmation_code     = md5(uniqid(mt_rand(), true));
        // Save if valid. Password field will be hashed before save
        $this->save($user);
        $membership_id = User::where('username',array_get($input, 'username'))->pluck('id');
        $number = sprintf("%04d", $membership_id);
        $final_number = date("Y")."-".$number;
        User::where('username',array_get($input, 'username'))->update(
            array(
                "membership_id" => $final_number
            )
        );
        return $user;
    }

    /**
     * Attempts to login with the given credentials.
     *
     * @param  array $input Array containing the credentials (email and password)
     *
     * @return  boolean Success?
     */
    public function login($input)
    {
        if (! isset($input['password'])) {
            $input['password'] = null;
        }

        return Confide::logAttempt($input, Config::get('confide::signup_confirm'));
    }

    /**
     * Checks if the credentials has been throttled by too
     * much failed login attempts
     *
     * @param  array $credentials Array containing the credentials (email and password)
     *
     * @return  boolean Is throttled
     */
    public function isThrottled($input)
    {
        return Confide::isThrottled($input);
    }

    /**
     * Checks if the given credentials correponds to a user that exists but
     * is not confirmed
     *
     * @param  array $credentials Array containing the credentials (email and password)
     *
     * @return  boolean Exists and is not confirmed?
     */
    public function existsButNotConfirmed($input)
    {
        $user = Confide::getUserByEmailOrUsername($input);

        if ($user) {
            $correctPassword = Hash::check(
                isset($input['password']) ? $input['password'] : false,
                $user->password
            );

            return (! $user->confirmed && $correctPassword);
        }
    }

    /**
     * Resets a password of a user. The $input['token'] will tell which user.
     *
     * @param  array $input Array containing 'token', 'password' and 'password_confirmation' keys.
     *
     * @return  boolean Success
     */
    public function resetPassword($input)
    {
        $result = false;
        $user   = Confide::userByResetPasswordToken($input['token']);

        if ($user) {
            $user->password              = $input['password'];
            $user->password_confirmation = $input['password_confirmation'];
            $result = $this->save($user);
        }

        // If result is positive, destroy token
        if ($result) {
            Confide::destroyForgotPasswordToken($input['token']);
        }

        return $result;
    }

    /**
     * Simply saves the given instance
     *
     * @param  User $instance
     *
     * @return  boolean Success
     */
    public function save(User $instance)
    {
        return $instance->save();
    }
}
