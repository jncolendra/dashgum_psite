<?php

class EventsTable extends Eloquent {
	protected $table = 'events';
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
}