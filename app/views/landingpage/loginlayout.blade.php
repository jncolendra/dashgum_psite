<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Philippine Society of Information Technology Educators</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('/css/bootstrap.css'); }}
    <!--external css-->
    {{ HTML::style('/font-awesome/css/font-awesome.css'); }}
    <!-- Custom styles for this template -->
    {{ HTML::style('/css/style.css'); }}
    {{ HTML::script('/js/jquery.js'); }}
    {{ HTML::style('/css/style-responsive.css'); }}
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <header class="header black-bg">
            <!--logo start-->
            <a href="/" class="logo"><b>Philippine Society of Information Technology Educators</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                    <li><a class="logout"  href="/users/create">Sign Up</a></li>
              </ul>
            </div>
        </header>

  
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	@yield('content')
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    
    {{ HTML::script('/js/bootstrap.min.js'); }}

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
     {{ HTML::script('/js/jquery.backstretch.min.js'); }}
    <script>
        $.backstretch("/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
