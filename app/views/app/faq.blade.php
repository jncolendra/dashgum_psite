@extends('app.layout')
@section('content')
	<div class="showback">
		<span style = "font-size:1.5em;color: #0080FF">Frequently Answered Questions</span><br/>
	<hr/>
		
		Q. How do I become a PSITE NCR member?</br>
		A. Just fill in the PSITE NCR Application for Membership Form and submit to any of the officers with your payment.</br>
		
		Q. How much is the individual membership fee?</br>
		A. The individual membership fee is PhP 500.00.</br>
		
		Q. How much is the institutional membership fee?</br>
		A. The institutional membership fee is PhP 3000.00.</br>
		
		Q. What are the benefits of being a member of PSITE NCR?</br>
		A. Please cllick on the "Membership Benefits" in the PSITE NCR Members section in the left panel of the website.</br>
		
		Q. How do I remit the payment for my membership?</br>
		A. There are several ways to remit your payment. You may pay the amount directly with any of the PSITE NCR officers, who will then forward your payment to the PSITE NCR Treasurer so an official receipt (OR) will be issued.  You may also deposit your payment to the bank, the transaction slip should be scanned and sent directly to the email address of the PSITE NCR Treasuer, who will then confirm if the payment has been reflected in the account.  An OR will then be issued to you, and may be given in the monthly seminar/workshop, or may be sent to you directly.  Another mode will be check payment, addressed to PSITE Foundation Inc.</br>
		
		Q. Which bank should I deposit my membership fee?</br>
		A. It is in any Allied Bank, the account details of which can be found in the PSITE NCR Members section, and clicking the "Payment for Membership".</br>
		
		Q. How do I renew my membership?</br>
		A. Just fill in the PSITE NCR Application for Membership Form and submit to any of the officers with your payment.  The instructions can also be found in the PSITE NCR Members section of the website, http://www.psite-ncr.org/.
	</div>	
@stop