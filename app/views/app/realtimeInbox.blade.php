	<?php $userconvos = TBMsg::getUserConversations(Confide::user()->id); ?>
	@foreach($userconvos as $userconvo)
		<?php
		$token = "";
        $lastMessage = $userconvo->getLastMessage();
          $participants= $userconvo->getAllParticipants();
          foreach ($participants as $key) {
            if (Confide::user()->id != $key){
              $token = $key;
            }  
          }
        $photo = User::where('id',$participants[1])->pluck('photofilename');
		?>
		@if($lastMessage->getStatus() == 1)
		<div style="background-color: #EBEBE0; padding-top: 0.25em;padding-left: 0.25em;">
		<img src="/profilephotos/{{$photo}}" height="50px" width="50px" style="float:left;">
		<a href="/sendMessage/{{$token}}">{{User::where('id',$token)->pluck('username')}}</a><br>
		<!-- Sender: {{User::where('id',$lastMessage->getSender())->pluck('username')}} <br> -->
		Message: {{substr($lastMessage->getContent(),0,40)}}... <br>
		{{$lastMessage->getcreated()}}
		</div>
		@elseif($lastMessage->getStatus() == 2)
		<div style="padding-top: 0.25em;padding-left: 0.25em;">
		<img src="/profilephotos/{{$photo}}" height="50px" width="50px" style="float:left;">
		<a href="/sendMessage/{{$token}}">{{User::where('id',$token)->pluck('username')}}</a><br>
		<!-- Sender: {{User::where('id',$lastMessage->getSender())->pluck('username')}} <br> -->
		Message: {{substr($lastMessage->getContent(),0,40)}}... <br>
		{{$lastMessage->getcreated()}}
		</div>
		@endif
	
		<hr>
	@endforeach