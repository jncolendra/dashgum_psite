<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    

    <title>PSITE - Network Portal</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('/css/bootstrap.css'); }}
    <!--external css-->
    {{ HTML::style('/font-awesome/css/font-awesome.css'); }}
    {{ HTML::style('/js/gritter/css/jquery.gritter.css'); }}
    {{ HTML::style('/lineicons/style.css'); }}    
    
    <!-- Custom styles for this template -->
    {{ HTML::style('/css/style.css'); }} 
    {{ HTML::style('/css/style-responsive.css'); }}
    {{ HTML::script('/js/chart-master/Chart.js'); }}    
    {{ HTML::script('/js/jquery.js'); }}
    {{ HTML::script('/js/debounce.js'); }}
      
    {{ HTML::script('/js/bootstrap.min.js'); }}
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="getOnline();">

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">

              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/dashboard" class="logo"><b>Philippine Society of Information Technology Educators</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->

                    <!-- inbox dropdown start-->

                    

                    <li id="header_inbox_bar" class="dropdown" data-toggle="tooltip-quickbox" data-placement="bottom" title="This is your Quick Access Inbox">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                            <i class="fa fa-envelope-o"></i>
                            <span id="msgCount" class="badge bg-theme">
                              <i class="fa fa-spinner fa-spin"></i>
                              {{--TBMsg::getNumOfUnreadMsgs(Confide::user()->id)--}}</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have <font id="msgCount2">{{TBMsg::getNumOfUnreadMsgs(Confide::user()->id)}}</font> new message/s</p>
                            </li>
                            <div id="rtlquickbox" style="overflow-y: scroll; height:160px;">
                              <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center>
                            </div>
                            <li>
                                <a href="/inbox">See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->

            <!-- inbox dropdown end -->

                </ul>
                   

                <!--  notification end -->
            </div>

            <div class="top-menu">



                <!-- Search Bar -->
              <form id="thisForm" class = "col-lg-2 col-md-2 col-sm-2 hidden-xs hidden-sm" method="post" style="padding-top:1em;" action = "/searchBox">
                  <input id="searchForm"  type="text" class="form-control" name="searchboxinput" autocomplete="off" placeholder="Search Here...">
                  <div id = "popUpResults" class = "showback col-md-12" style="display:none;position:absolute; "> <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center></div>
              </form>
              

              <script>
                $('#searchForm').keyup(function(e){
                            $('#popUpResults').show("fast");
                            if(e.keyCode == 8){
                            /*alert("backspace");*/
                            $('#popUpResults').html("<center><br><i class='fa fa-spinner fa-spin'></i><br><br>Enter keyword..</center>");
                            /*setInterval(Ajax(),700);*/
                            }
                });
                $('#searchForm').keyup($.debounce(function(){
                  $('#popUpResults').html("");
                  Ajax();
                },500));
                  function Ajax(){
                    $.ajax({
                               type: 'POST',
                                url: '/searchBox',
                                data: $('#thisForm').serialize(),
                                dataType: "json",//{ searchboxinput: x },
                                success:function(results){
                                      
                                      for (x in results[0])
                                      {
                                        $('#popUpResults').append("<img height='35' width='35' src='/profilephotos/"+results[0][x].photofilename+"'><a href='/profile/"+results[0][x].username+"'>"+results[0][x].username+"</a><br/>");  
                                      }

                                      for (y in results[1])
                                      {
                                        $('#popUpResults').append("<img height='35' width='35' src='/eventphotos/"+results[1][y].event_photo+"'><a href='/events/"+results[1][y].id+"'>"+results[1][y].event_title+"</a><br/>");
                                      }
                                }
                          });
                  }
                  $('#searchForm').blur(function(){
                      $('#popUpResults').hide("slow");

                  });
              </script>





           
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="/users/logout"><i class="fa fa-times fa-lg"></i> Logout</a></li>
              </ul>
              @if (Confide::user()->is_admin == 1)
              <ul class="nav pull-right top-menu" data-toggle="tooltip-administration" data-placement="bottom" title="Brings you to the Administration Panel of PSITE!">
                    <li><a class="logout" href="/admin"><i class="fa fa-key fa-lg"></i> Administration</a></li>
              </ul>
              @endif
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered">
                    <img src="/profilephotos/{{Confide::user()->photofilename}}" class="img-circle" height= "60" width="60" id="btnphoto"  data-toggle="tooltip-upload" title="Click the picture here to upload your own!" data-placement="bottom">
                    <form id="photoform" action="/uploadPhoto" method="POST" enctype="multipart/form-data">
                      <input id="uploadphoto" type="file" name="file" capture="camera" style="display: none;">
                    </form>
                  </p>
              	  <h5 class="centered">{{Confide::user()->username ?: 'visitor';}}</h5>
              	  <li class="mt">
                      <a href="/dashboard" data-toggle="tooltip-feed" title="Brings you to Member's Event Updates!">
                          <i class="fa fa-rss"></i>
                          <span>Feed</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="/profile/{{Confide::user()->username}}" data-toggle="tooltip-profile" title="Brings you to your own Profile Page!">
                          <i class="fa fa-user"></i>
                          <span>Profile</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="/inbox" data-toggle="tooltip-messages" title="Brings you to your own Inbox!">
                          <i class="fa fa-envelope"></i>
                          <span>Messages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="/faq" data-toggle="tooltip-messages" title="Frequently Answered Questions">
                          <i class="fa fa-question"></i>
                          <span>FAQ</span>
                      </a>
                  </li>
                  <script type="text/javascript">
                  $('[data-toggle="tooltip-feed"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  $('[data-toggle="tooltip-profile"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  $('[data-toggle="tooltip-messages"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  $('[data-toggle="tooltip-administration"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  $('[data-toggle="tooltip-quickbox"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  $('[data-toggle="tooltip-upload"]').tooltip({delay: { "show": 250, "hide": 100 }});
                  </script>
                  
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-9 main-chart">
                    @yield('content')
					
										
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              <div class="col-lg-3 ds">
              <h3><span id="notifcount" class="badge bg-theme"><i class="fa fa-spinner fa-spin"></i></span>NOTIFICATIONS</h3>
              
              <br>
                <div id="notification" style="height: 160px; overflow-y: scroll;">  
                <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center>  
                </div>
              
              <h3>Who's Online</h3>
              <br>
                <div id="online" style="height: 198px; overflow-y: scroll;">
                  <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center>
                </div>
                          
              </div><!-- /col-lg-3 -->
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Capstone Project 2015 - Trinity University of Asia by Colendra, Villareal, Marticio, Soliman
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    {{ HTML::script('/js/jquery.dcjqaccordion.2.7.js'); }}
    {{ HTML::script('/js/jquery.scrollTo.min.js'); }}
    {{ HTML::script('/js/jquery.nicescroll.js'); }}
    {{ HTML::script('/js/jquery.sparkline.js'); }}

    <!--common script for all pages-->
    {{ HTML::script('/js/common-scripts.js'); }}
    {{ HTML::script('/js/gritter/js/jquery.gritter.js'); }}
    {{ HTML::script('/js/gritter-conf.js'); }}
    
    <!--script for this page-->
    {{ HTML::script('/js/sparkline-chart.js'); }}
	
	<script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            
            title: 'Welcome to PSITE Network Portal, {{Confide::user()->username}}!',
            
            text: 'Hover me to enable the Close Button. Capston Project created by Colendra, Villareal, Marticio, Soliman',
            
            image: '{{URL::to('profilephotos/'.Confide::user()->photofilename)}}',//change this to profile photo
            
            sticky: false,
            
            time: '800',
            
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script>

  <!-- Coded Scripts -->
  <script>
      $(window).on('load', function(){
      $.ajax("/active");
      });
      $(window).on('unload', function(){
      $.ajax("/idle");
      });
      setInterval(function(){
        $.ajax({
          type:"get",
          url:"/msgcount",
          timeout: 5000,
          success : function(count){
            $("#msgCount").html(count);
            $("#msgCount2").html(count);
              $.ajax({
                type:"get",
                url:"/notifcount",
                timeout: 5000,
                success : function(count){
                  $("#notifcount").html(count);
                  $("#notification").load("/getnotif");
                  $("#rtlquickbox").load("/quickbox");
                }
              });
          }
        });
      },5000);
  /*Upload Photo jQuery*/
      $('#btnphoto').click(function(){
          $('#uploadphoto').click();
        });
      $('#uploadphoto').change(function(){
        $('#photoform').submit();
      });

  setInterval(function(){getOnline()},5000); //Get Online Users Every 30secs
  /*Who Is Online - Ajax*/
      function getOnline(){
            $.ajax({
              type: 'GET',
              url: '/getonline',
              success:function(online){
                $('#online').html('');
                  for(x in online){
                      $('#online').append(
                        "<div class='desc'><div class='thumb'><img class='img-circle' width='35' height='35' src='/profilephotos/"+online[x].photofilename+"'></div><div class='details'><p><a href='/profile/"+online[x].username+"'>"+online[x].username+"</a></br><muted>AVAILABLE</muted></p></div></div>"
                        );
                  }
              }
            });
      }
  </script>
	

  </body>
</html>
