@extends('app.layout')
@section('content')
       	<?php
       	$event_first = DB::table('events')->orderBy('created_at')->first();
       	$events = DB::table('events')->skip(1)->take(5)->orderBy('created_at')->get();
       	$x = 1;
       	?>
		<div class="col-lg-12 col-md-12 col-sm-12 mb">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators" style="bottom: -2px;">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		  	@foreach($events as $event)
		    <li data-target="#carousel-example-generic" data-slide-to="{{$x}}"></li>
		    <?php
		    	$x++;
		    ?>
		    <!-- <li data-target="#carousel-example-generic" data-slide-to="2"></li> -->
		    @endforeach
		  </ol>


		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      	<img src="/eventphotos/{{$event_first->event_photo}}" style="display:block;margin:auto;height:400px !important;">
		      	<div class="carousel-caption" style="background-color: black; opacity: 0.8; filter: alpha(opacity=40); width:100%; position: absolute; left: 0; bottom: 0;">
		      		<div style="color:white;" width="100%">
					    <h3>{{$event_first->event_title}}</h3>
					    <font size= "2">When: </font>{{$event_first->event_date}}<br>
		              	<font size= "2">Where: </font>{{$event_first->event_place}}<br>
		              	{{substr($event_first->event_description, 0, 25)}} ...<a href="/events/{{$event_first->id}}">Read More</a>
	              	</div>
				</div>
		    </div>
		    @foreach ($events as $event)
		    	<div class="item">
		      			<img src="/eventphotos/{{$event->event_photo}}" style="display:block;margin:auto;height:400px !important;">
				      	<div class="carousel-caption" style="background-color: black; opacity: 0.8; filter: alpha(opacity=40); width:100%; position: absolute; left: 0; bottom: 0;">
				      		<div style="color:white;" width="100%">
							    <h3>{{$event->event_title}}</h3>
							    <font size= "2">When: </font>{{$event->event_date}}<br>
				              	<font size= "2">Where: </font>{{$event->event_place}}<br>
				              	{{substr($event->event_description, 0, 25)}} ...<a href="/events/{{$event->id}}">Read More</a>
			              	</div>
						</div>
		    	</div>
		    @endforeach
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
		</div>

		<!-- Membership Benefit Column -->
		<div class="col-lg-6 col-md-6 col-sm-6 mb">
			<div class = "content-panel" style="padding:1em;height: 300px;">
        		<i class = "fa fa-star fa-2x" style="padding-right:5px;"></i><span style="font-size:1.5em;">Membership Benefits</span>
        		<hr style="background-color: #3498db;height:2px;">
        		<h4>There are 3 kinds of members in this organization:</h4>
        		 <blockquote>
        			<h5>• Institutional</h5>
        			<h5>• Individual</h5>
        			<h5>• Corporate/Industry</h5>
        		</blockquote>
        		<span>To learn more about membership benefits</span><a href="/membershipbenefits"><button class = "btn btn-primary btn-xs" style="float:right;">Read More</button></a>
        		
        	</div>
        </div>

        <!-- About Us Column --> 
        <div class="col-lg-6 col-md-6 col-sm-6 mb">
        	<div class = "content-panel" style="padding:1em;height: 300px;">
        		<i class = "fa fa-male fa-2x" style="padding-right:5px;"></i><span style="font-size:1.5em;">About Us</span>
        		<hr style="background-color: #3498db;height:2px;  margin-bottom: 14px;">
        		<span>The Philippines Society of Information Technology Educators Foundation, Inc. (PSITE) is an organization composed of academic institutions, IT program heads, faculty members, corporate members, and IT graduate students.  It aims to promote quality Information Technology education in the country through workshops, seminars, industry-academe linkages, and programs designed to benefit its members, and the academic sector in general.
				PSITE has fourteen regional chapters, more than 100 institutional members and 900+ individual members....
			</span>
			<br/>
			<a href = "/aboutus"><button class = "btn btn-primary btn-xs" style="float:right;">Read More</button></a>
		
        	</div>
        </div>

@stop