@extends('app.layout')
@section('content')

		<div class = "content-panel" style = "padding: 10px;"> 
			<span style = "font-size: 1.5em;color: #0080FF"><b>Philippine Society of Information Technology Educators, Foundation, Inc.</b></span><br/>
			<span style = "font-size: 1.5em;">National Capital Region Chapter</span>
			
			<hr/>

			<p>The Philippines Society of Information Technology Educators Foundation, Inc. (PSITE) is an organization composed of academic institutions, IT program heads, faculty members, corporate members, and IT graduate students.  It aims to promote quality Information Technology education in the country through workshops, seminars, industry-academe linkages, and programs designed to benefit its members, and the academic sector in general.
			PSITE has fourteen regional chapters, more than 100 institutional members and 900+ individual members.  The PSITE National Capital Region (NCR) chapter is one of the biggest in terms of membership.  PSITE NCR provides monthly seminars and workshops, fora, tie-ups with industry practitioners, and programs for professional, technological, pedagogical, and personal/social development of its members.
			</p>

			<hr/>

			<span style = "font-size: 1.5em;">Vision</span>
			<p>
				The premier national IT Professional organization transforming the academic community that enables and sustains the growth of a knowledge-drive Philippine economy.
			</p>

			<hr/>

			<span style = "font-size: 1.5em;">Mission</span>
			<p>
				We are committed in providing opportunities for the development of its members through:
					<ol type = i>
						<li>Strong community and <b><i>Partnership</i></b> involvement with the education providers, industry, government, non-government agencies and international counterparts</li>
						<li>Dynamic and responsive <b><i>Educational</i></b> initiatives for an enhanced academic standard.</li> 
						<li>Relevant community <b><i>Extension</i></b> and immersion programs.</li>
						<li>Aggresive building of <b><i>Research Capability</i></b> and effective mentoring system.</li>
					</ol>
			</p>

			<hr/>
			<span style = "font-size: 1.5em;">Values</span>
				<ol type = i>
					<li>Professionalism</li>
					<li>Ethical</li>
					<li>Service Oriented</li>
					<li>Passion for teaching and learning</li>
					<li>Passion for lifelong learning</li>
		</div>

@stop
