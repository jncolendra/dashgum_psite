<?php $userconvos = TBMsg::getUserConversations(Confide::user()->id);?>
                            @foreach($userconvos as $userconvo)
                            <?php
                            $token = "";
                            $lastMessage = $userconvo->getLastMessage();
                              $participants= $userconvo->getAllParticipants();
                              foreach ($participants as $key) {
                                if (Confide::user()->id != $key){
                                  $token = $key;
                                }  
                              }
                            $photo = User::where('id',$participants[1])->pluck('photofilename');
                            ?>
                            <li>
                                <a  href="/sendMessage/{{$token}}">
                                    <span class="photo"><img alt="avatar" src="/profilephotos/{{$photo}}" height="50px" width="50px"></span>
                                    <span class="subject">
                                    <span class="from">{{User::where('id',$token)->pluck('username')}}</span>
                                    </span>
                                    <span class="message">
                                        {{substr($lastMessage->getContent(),0,10)}}...
                                    </span>
                                    <span class="time">{{$lastMessage->getcreated()}}</span>
                                </a>
                            </li>
                            @endforeach