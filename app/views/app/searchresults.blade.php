@extends('app.layout')
@section('content')
<?php 
$users = DB::table("users")->where("username", "LIKE" , "%".Session::get("query")."%")->simplePaginate(1);
$events = DB::table("events")->where("event_title","LIKE","%".Session::get("query")."%")->simplePaginate(1);
?>
		<div class="showback">
						<div class="btn-group btn-group-justified">
						  		<div class="btn-group">
						    		<button id = "peopleBtn" type="button" class="btn btn-theme active">People</button>
						  		</div>
						
						 	 	<div class="btn-group">
						    		<button id = "eventsBtn" type="button" class="btn btn-theme">Events</button>
						  		</div>
						</div>

								<!-- people section -->
								<div id = "peopleSection" style="opacity: 0.8; filter: alpha(opacity=40);">
									@if($users != NULL)
									@foreach($users as $result)
									<div class="commonattr" style="display:inline-block;">
										<div style="float:left; margin:1em;">
											<img src="/profilephotos/{{$result->photofilename}}" height="100px" width="100px">
										</div>
										<div style="float:left;margin:1em; font-size: 1.2em;">
											<span class="Username1"><b>Name: </b>{{$result-> individual_fname}}&nbsp;{{$result-> individual_lname}}</span><br/>
											<span><b>E-mail: </b> {{$result->email}} </span><br/>
											<span><b>Membership Type: </b>{{$result->usertype}}</span><br/>
											<span><b>Position: </b>{{$result->username}}</span><br/>
											<span><a href="/profile/{{$result->username}}">View Profile</a></span>
										</div>
									</div>
									<hr>
									@endforeach
									{{$users->links()}}
									@endif

									@if($users == NULL)
									<div class="alert alert-danger">
						            <center><b>No Records Found!<b></center>
						          	</div>
									@endif
								</div>

								<!-- event section -->
								<div id = "eventSection" style="opacity: 0.8; filter: alpha(opacity=40);display:none;">
									@if($events != NULL)
										@foreach($events as $result)
									<div class="commonattr" style="display:inline-block;">
										<div style="float:left; margin:1em;">
											<img src="/eventphotos/{{$result->event_photo}}" height="100px" width="100px">
										</div>
										<div style="float:left;margin:1em; font-size: 1.2em;">
											<span class="Username1"><b>Event Title: </b>{{$result->event_title}}</span><br/>
											<span><b>Date: </b> {{$result->event_date}} </span><br/>
											<span><a href="/events/{{$result->id}}">Read More</a></span>
										</div>
									</div>
									<hr>
									@endforeach
									{{$events->fragment('eventsBtn')->links()}}
									@endif
									@if($events == NULL)
									<div class="alert alert-danger">
						            <center><b>No Records Found!<b></center>
						          	</div>
									@endif
								</div>

								<script>
									$(window).ready(function(){
										if(document.location.href.indexOf('#eventsBtn') > -1){
											$('#peopleBtn').attr('class', 'btn btn-theme');
										$('#peopleSection').hide("slow");
										$('#eventSection').show("slow");
										}
									});
									$('#eventsBtn').click(function(){
										$(this).attr('class', 'btn btn-theme active');
										$('#peopleBtn').attr('class', 'btn btn-theme');
										$('#peopleSection').hide("slow");
										$('#eventSection').show("slow");
									});

									$('#peopleBtn').click(function(){
										$(this).attr('class', 'btn btn-theme active');
										$('#eventsBtn').attr('class', 'btn btn-theme');
										$('#peopleSection').show("slow");
										$('#eventSection').hide("slow");
									});
								</script>     				
      	</div>
		
@stop