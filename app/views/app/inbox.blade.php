@extends('app.layout')
@section('content')
	<div class="showback">
		<h4><i class="fa fa-angle-right"></i> Inbox</h4>
		<hr>
		<div id="inbox">
			<center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center> 
		</div>
	</div>
	<script type="text/javascript">
	setInterval(function(){
		$("#inbox").load("/rltinbox");
	},5000);
	</script>
@stop
