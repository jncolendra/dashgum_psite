<?php
                $notification = DB::table('notifications')->where('receiver_id',Confide::user()->id)->orderBy('id', 'desc')->get();
                
                ?>
@if($notification == null)
  <center>You have no notifications as of now... </center>
@endif
@foreach ($notification as $key)
                          <?php
                             $render = DB::table('notification_types')->where('id',$key->type_id)->first();
                             $eventGetter = DB::table('events')->where('event_title', $key->extra)->first();
                          ?>
                          @if ($key->type_id==1)
                            @if($key->state == 1)
                              <div class="desc" style="background-color: #EBEBE0;">
                                <div class="thumb">
                                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                              </div>
                              <div class="details">
                                 <a href="/readNotifs/{{$key->id}}/{{$eventGetter->id}}/{{$key->type_id}}">{{$render->message.$key->extra}}</a><br>
                                <p><muted>{{$key->created_at}}</muted><br>
                                </p>
                              </div>
                              </div>
                            @endif
                            @if($key->state == 0)
                              <div class="desc">
                                <div class="thumb">
                                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                              </div>
                              <div class="details">
                                 <a href="/readNotifs/{{$key->id}}/{{$eventGetter->id}}/{{$key->type_id}}">{{$render->message.$key->extra}}</a><br>
                                <p><muted>{{$key->created_at}}</muted><br>
                                </p>
                              </div>
                              </div>
                            @endif
                          @endif
                          
                          @if(Confide::user()->is_admin == 1)
                          @if ($key->type_id==2)
                            @if($key->state == 1)
                              <div class="desc" style="background-color: #EBEBE0;">
                                <div class="thumb">
                                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                              </div>
                              <div class="details">
                                 <a href="/readNotifs/{{$key->id}}/{{$key->extra}}/{{$key->type_id}}">{{$key->extra.' '.$render->message}}</a><br>
                              <p><muted>{{$key->created_at}}</muted><br>
                              </p>
                              </div>
                              </div>
                            @endif
                            @if($key->state == 0)
                              <div class="desc">
                                <div class="thumb">
                                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                              </div>
                              <div class="details">
                                 <a href="/readNotifs/{{$key->id}}/{{$key->extra}}/{{$key->type_id}}">{{$key->extra.' '.$render->message}}</a><br>
                              <p><muted>{{$key->created_at}}</muted><br>
                              </p>
                              </div>
                              </div>
                            @endif
                          @endif
                          @endif
                  @endforeach