@extends('app.layout')
@section('content')
	<div class="showback">
	<img src="/eventphotos/{{$events->event_photo}}" style="width: 50%; position: inherit; margin-left: 217px; box-shadow: -4px 5px 6px gray;">
		<h2 align="center"><b><i>{{$events->event_title}}</i></b></h2>
		<div class="showback2" style="padding-left: 15px;">
		<h4><br><b>Date:</b></h4><p style="font-size: 15px;">{{$events->event_date}}</p>
		<h4><b>Venue:</b></h4><p style="font-size: 15px;">{{$events->event_place}}</p>
		<h4><b>Description:</b></h4><p style="font-size: 15px;">{{$events->event_description}}</p>
	</div></div>
@stop