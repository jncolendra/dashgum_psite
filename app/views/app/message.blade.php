@extends('app.layout')
@section('content')
	<div class="col-lg-12 col-md-12 col-sm-12" >
        <div class="content-panel">
            
            <h4><i class="fa fa-angle-right"></i> Conversation With:  <b> {{$receiver->username}} </b>
        </div>
    </div>
    <br>
    <br>
	 <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="content-panel"><hr>
        <div id="loadMsg" style="overflow-y: scroll; height:300px;">
        <?php 
        $messages = $messages->reverse();
        ?>  
      	@foreach($messages as $message)
          <img src="/profilephotos/{{User::where('id', $message->getSender())->pluck('photofilename')}}" width="40" height="40">
         &nbsp;&nbsp;
          @if($message->getSender() == Confide::user()->id)
          <b>You:</b>
          @elseif($message->getSender() != Confide::user()->id)
          <b>{{User::where('id', $message->getSender())->pluck('username')}}: </b>
          @endif
          {{$message->getContent()}} <br>

           <br>
        <div class="datecreated" style="text-align: center; color: #bdc1c9;">
        
        {{$message->getcreated()}} 
        </div>
        @endforeach
        <div id="bottomMSG"></div>
        </div>
      </div>
    </div>

	<div class="col-lg-12 col-md-12 col-sm-12" >
                      <div class="content-panel">
                      	<form id="sendForm" action="/deliverMessage" method="post" style="padding: 1em;">
                      		<input type="hidden" name="receiverId" value="{{$receiver->id}}">
                      		<input id="txtMsg" class="form-control" name="message" type="text" placeholder="Write a reply...">
                      	</form>
                      </div>
    </div>
    <script type="text/javascript">
    $("#sendForm").submit(function(e){
      e.preventDefault();
      if (e.keyCode = 13){
        if($("#txtMsg").val() != ""){
          sendMessages();
          $("#txtMsg").val("");
        }else{
          alert("You cannot send blank messages!");
        }
      } 
    });
    function sendMessages(){
      $.ajax({
        type:'POST',
        url:"/deliverMessage",
        data:$("#sendForm").serialize(),
        success:function(){
          $("#loadMsg").load("/sendMessage/{{$receiver->id}}");
        }
      });
    }
    $(window).load(function(){
       $("#loadMsg").animate({
        scrollTop: $("#bottomMSG").offset().top
       },1000);
    });
    setInterval(function(){
      $("#loadMsg").load("/sendMessage/{{$receiver->id}}");
    },5000);
    </script>
@stop