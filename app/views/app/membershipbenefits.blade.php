@extends('app.layout')
@section('content')



<div class = "content-panel" style = "padding:1em;">
	<span style = "font-size:1.5em;color: #0080FF">SUMMARY OF MEMBERSHIP BENEFITS</span><br/>
	<hr/>
	<span style = "font-size:1.25em;"><b>Institutional Members</b></span>
		<br>
		<ol type = "a">
			<li>Opportunity to attend Monthly Seminar Workshops (MSW)</li>
			<li>Outstanding student award</li>
			<li>Access to curriculum and instructional materials</li>
			<li>Avenue for research forum</li>
			<li>Opportunity to join immersion trips</li>
			<li>Discounts on Masters Studies at selected graduate schools</li>
			<li>Tie up with software industry partners such as Sun Microsystems, Microsoft, etc.</li>
			<li>Tie up with other IT academic programs such as Sun Java Education and Developmetn Initiative (JEDI), PhilNITS, Philippine Open Source Initiative (POSITIVE), and others</li>
			<li>Participation in various fora for knowledge exchange</li>
			<li>Opportunity to attend conventions</li>
		</ol>
	<hr/>
	<span style = "font-size:1.25em;"><b>Individual Members</b></span>
		<ol type = "a">
			<li>Opportunity to attend Monthly Seminar Workshops</li>
			<li>Access to curriculum and instructional materials</li>
			<li>Avenue for research forum</li>
			<li>Opportunity to join immersion trips</li>
			<li>Participation in various fora for knowledge exchange</li>
			<li>Opportunity to attend conventions</li>
		</ol>
	<hr/>

	<span style = "font-size:1.25em;"><b>Corporate/Industry Members</b></span>
		<ol type = "a">
			<li>Venue for supply of On-the-job training students</li>
			<li>Venue for supply of prospective applicants</li>
			<li>Avenue for product and technological presentation</li>
			<li>Customize training package for employee skill upgrade</li>
		</ol>
	</div>
@stop