@extends('app.layout')
@section('content')

	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="showback">
				<img src="/profilephotos/{{$profileData->photofilename}}" align="left" width="160" height="160">
						
				<!-- Button -->
				
				<div style="padding-left: 159px;">
				<div class="btn-group" style="float: right;">
					@if($profileData->username != Confide::user()->username)
					<button type="button" class="btn btn-theme03">Action</button>
					<button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
					</button>
					@endif

					<ul class="dropdown-menu" role="menu">
					  	<li><a  href='/sendMessage/{{$profileData->id}}'>New Message</a></li>
					</ul>
				</div>
				
				<!-- Common Attributes -->
				
				<div class="commonattr" style="margin-left: 30px;">
				<h3 class="Username1"><b>{{$profileData->username}}</b></h3> 
				<h4><b>E-mail:</b> {{$profileData->email}} </h4>
				<h4><b>Membership Type: </b>{{$profileData->usertype}}</h4>
				<h4><b>Position: </b>{{$profileData->officer_position}}</h4> 
				</div>
				<br>
				</div>
				
				
				<!-- Individual Attributes -->
				@if($profileData->usertype == "individual")
				<div>
				<h3 style="background-color: aliceblue; padding: 5px;">Individual Membership</h3>
				<div style="padding-left: 40px; margin-top: 15px;">
				<h4><b>Full Name:</b> {{$profileData->individual_fname." ".$profileData->individual_lname}}</h4>
				<h4><b>Individual Affiliation:</b> {{$profileData->individual_affiliation}}</h4>
				<h4><b>Individual President:</b> {{$profileData->individual_president}}</h4>
				<h4><b>Highest Educational Attainment:</b> {{$profileData->individual_attainment}}</h4>
				</div>
				</div>
				@endif
				
				
				<!-- Institutional Attributes -->
				@if($profileData->usertype == "institutional")
				<div>
				<h3 style="background-color: aliceblue; padding: 5px;">Institutional Membership</h3>
				<div style="padding-left: 40px; margin-top: 15px;">
				<h4><b>Institution Affiliation:</b> {{$profileData->institution_affiliation}}</h4>
				<h4><b>Institution Address:<b/> {{$profileData->institution_address}}</h4>
				<h4><b>Institution President:</b> {{$profileData->institution_president}}</h4>
				<hr>
				<h4><b>Representative Name No. 1:</b> {{$profileData->institution_rep1_name}}</h4>
				<h4><b>Representative Contact:</b> {{$profileData->institution_rep1_contact}}</h4>
				<h4><b>Representative Name No. 2:</b> {{$profileData->institution_rep2_name}}</h4>
				<h4><b>Representative Contact:</b> {{$profileData->institution_rep2_contact}}</h4>
				</div></div>
				@endif
				

				<!-- Corporate Attributes -->
				@if($profileData->usertype == "corporate")
				<div>
				<h3 style="background-color: aliceblue; padding: 5px;">Corporate Membership</h3>
				<div style="padding-left: 40px; margin-top: 15px;">
				<h4><b>Company Name:</b> {{$profileData->corporate_companyname}}</h4>
				<h4><b>Company Address:</b> {{$profileData->corporate_address}}</h4>
				<h4><b>Company's Representative Name:</b> {{$profileData->corporate_rep}}</h4>
				<h4><b>Company's Representative Contact:</b> {{$profileData->corporate_contact}}</h4>
				</div></div>
				@endif

			
				<!-- Seminars Attended Attributes -->
				<div>
				<h3 style="background-color: aliceblue; padding: 5px;">Seminars/Events Attended</h3>
					
							<ul>


							<?php 
								// echo $profileData->id;
								$eventattendance = DB::table('event_attendance')->where('user_id', $profileData->id)->get();
								foreach ($eventattendance as $result) {
								print '<h4><b><li>'.DB::table('events')->where('id', $result->event_id)->pluck ('event_title').'</li></b></h4>';
								}
								// dd ($eventattendance);
							?>

					</ul>
						<!-- 	<ul> 
						<li style="font-size: 18px;"> • Birthday ni Cla</li>
						<li style="font-size: 18px;"> • Birthday ni Gelai</li>
						<li style="font-size: 18px;"> • Birthday ni Niko</li>
						<li style="font-size: 18px;"> • Birthday ni Jae</li>
						<li style="font-size: 18px;"> • Birthday ni Keno</li>
						</ul> -->
				</div>
		</div>
	</div>

@stop