<h1>Approval of Your Account</h1>

<p>Hello {{$username}},</p>

<p>We have received your payment for your membership, and you are now officially a Member of the Philippine Society of Information Technology Educators, your registered account at <a href="{{ URL::to("/") }}">{{{ URL::to("/") }}}</a> is now eligible for usage. To check please login your registered account.</p>