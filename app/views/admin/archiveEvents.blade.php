@extends('admin.layout')
@section('content')
<?php
$eventArchive = EventsTable::onlyTrashed()->get();
?>
	<div class="showback">
		<span style = "font-size:1.5em;color: #00549F"> Archived Events</span><hr>
		<table class="table table-striped table-advance table-hover">
		 	<thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Event ID:</th>
                                  <th> Title:</th>
                                  <th> Date:</th>
                                  <th> Event Description</th>
                              </tr>
            </thead>
            <tr>
            	@foreach($eventArchive as $result)
	            	<td>
	            	{{$result->id}}
	            	</td>
	            	<td>
	            	{{$result->event_title}}
	            	</td>
	            	<td>
	            	{{$result->event_date}}
	            	</td>
	            	<td>
	            	{{$result->event_description}}
	            	</td>
	            	<td></td>
            	@endforeach
            </tr>
         </table>
	</div>

@stop