@extends('admin.layout')
@section('content')
<div class="showback">
	                            <span style = "font-size:1.5em;color: #00549F"> List of Individual Members</span><hr><table class="table table-striped table-advance table-hover">
	                  	  	  
	                              <thead>
	                              <tr>
	                                  <th>#</th>
	                                  <th>Affiliation</th>
	                                  <th>President</th>
	                                  <th>Representative 1</th>
	                                  <th>Contact</th>
	                                  <th>Representative 2</th>
	                                  <th>Contact</th>
	                                  <th>E-Mail</th>
	                                  <th></th>
	                              </tr>
	                              </thead>
	                              <tbody>
	                              @foreach($users as $user)
	                              <tr>
	                              	  <td>{{$user->membership_id}}</td>	
	                                  <td>{{$user->institution_affiliation}}</td>
	                                  <td>{{$user->institution_president}}</td>
	                                  <td>{{$user->institution_rep1_name}}</td>
	                                  <td>{{$user->institution_rep1_contact}}</td>
	                                  <td>{{$user->institution_rep2_name}}</td>
	                                  <td>{{$user->institution_rep2_contact}}</td>
	                                  <td>{{$user->email}}</td>
	                                  <td><a href="/profile/{{$user->username}}">View Profile</a></td>
	                              </tr>
	                              @endforeach
	                              </tbody>
	                          </table>
	                  	  </div>
@stop