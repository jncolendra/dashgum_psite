@extends('admin.layout')
@section('content')

						<div class="content-panel">
                            <span style = "font-size:1.5em;color: #00549F"> List of Confirmed Users</span><hr><table class="table table-striped table-advance table-hover">
                            
                            
                              <thead>
                              <tr>
                              	  <th><i class="fa fa-asterisk"></i>Membership ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i> E-Mail</th>
                                  <th><i class="fa fa-calendar"></i> Member Since:</th>
                                  
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($users as $user)
	                              <tr>
	                              		<td>
	                              			{{$user->membership_id}}
	                              		</td>
										<td>
											{{$user->username}}
	                              		</td>
										<td>
											{{$user->email}}
	                              		</td>
										<td>
											{{$user->created_at}}
	                              		</td>
	                              </tr>
                              @endforeach

                              </tbody>
                          </table>
                      </div>
@stop