@extends('admin.layout')
@section('content')
<?php
$events = DB::table('events')->get();
?>
<div class="showback">
	<span style = "font-size:1.5em;color: #00549F"> Event Attendance</span><hr><table class="table table-striped table-advance table-hover">
	                  	  	  
	                          <thead>
	                          	<tr>
	                          		<th>#</th>
	                          		<th>Event Title</th>
	                          		<th>Event Date</th>
	                          		<th>Event Description</th>
	                          		<th></th>
	                          	</tr>
	                          </thead>
	                          <tbody>
	@foreach($events as $event)                         	
	<tr>
		<td>{{$event->id}}</td>
		<td>{{$event->event_title}}</td>
		<td>{{$event->event_date}}</td>
		<td>{{substr($event->event_description,0,35)}}...</td>
		<td><center><button class="btn btn-primary btn-sm" onclick="ViewEvent({{$event->id}})">View Attendance</button></center></td>
	</tr>
		<?php 
		$attendees = DB::table('event_attendance')->where('event_id',$event->id)->get();
		?> 
		<tr id="viewAttendance{{$event->id}}" style="display:none;">
			<td colspan="5">
			<b><center>List of Attendees in {{$event->event_title}}:</center></b><hr>
			<div>
				<div class="col-lg-4">
				</div>
				<div class="col-lg-2">
				<b>Username:</b>
				</div>
				<div class="col-lg-6">
				<b>Commands:</b>
				</div>
			</div>
			<br>
			@foreach($attendees as $attendee)
					<div class="col-lg-4">
						
					</div>
					<div class="col-lg-2">
					{{User::where('id',$attendee->user_id)->pluck('username')}}
					</div>
					<div class="col-lg-6">	
					<div class="btn-group">
                        <button type="button" class="btn btn-theme03 btn-xs dropdown-toggle" data-toggle="dropdown">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="/certAttendance/{{$event->id}}/{{$attendee->user_id}}" target="_blank">View Certificate</a></li>
                          <li><a data-toggle="modal" data-target="#deleteModal{{$event->id.$attendee->user_id}}">Delete Attendance</a></li>
                          
                        </ul>
                    </div>
					</div>
					<div class="modal fade" id="deleteModal{{$event->id.$attendee->user_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					        <h4 class="modal-title" id="myModalLabel">Delete</h4>
					      </div>
					      <div class="modal-body">
					        Are you sure you want to approve the user: "{{(User::where('id',$attendee->user_id)->pluck('username'))}}"?
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					         <a href="/deleteAttendance/{{$event->id}}/{{$attendee->user_id}}"><button class="btn btn-primary">Delete Attendance </button></a>
					        </div>
					    </div>
					  </div>
					</div>

					<!-- <a href="/certAttendance/{{$event->id}}/{{$attendee->id}}" class="btn btn-primary btn-xs">View Certificate</a> -->
					<!-- <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#deleteModal{{$attendee->id}}">
					                           Delete Attendance
					</button> -->
				</br>
				
			@endforeach
			</td>
		</tr>
	@endforeach
	                          </tbody>

	                      </table>
</div>
<script>
 function ViewEvent(event_id){
 	var x="#viewAttendance"+event_id;
 	$(x).toggle("slow");
 }
</script>
@stop