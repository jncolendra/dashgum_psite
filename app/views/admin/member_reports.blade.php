@extends('admin.layout')
@section('content')
<?php 
$ind = DB::table('users')->where('usertype',"individual")->count();
$ins = DB::table('users')->where('usertype',"institutional")->count();
$cor = DB::table('users')->where('usertype',"corporate")->count();
?>
{{HTML::script("/js/chart-master/Chart.js")}}

<div class="showback">
	<span style = "font-size:1.5em;color: #00549F"> Membership Reports</span>
	                  	  	  
	<div class="alert alert-info">
		<div style="float:right;">
			<center>
			<canvas id="myChart"></canvas><br>
			<b>Pie Chart of Membership Types<b>
			</center>
		</div>
	
	
	<h4>Reports on Members according to type:</h4>
	<br>
	<blockquote><b>Individual Member/s:</b> {{$ind}}<br>
		<b>Institutional Member/s:</b> {{$ins}}<br>
		<b>Corporate Member/s:</b> {{$cor}}<br>
	</blockquote>
	
	<hr>
	The Pie chart on the right can be saved as an image for various uses.
	</div>
<script>
var data = [
    {
        value: {{$ind}},
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Individual Members"
    },
    {
        value: {{$ins}},
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Institutional Members"
    },
    {
        value: {{$cor}},
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Corporate Members"
    }
]
var ctx = document.getElementById("myChart").getContext("2d");
var myNewChart = new Chart(ctx).Pie(data);
new Chart(ctx).Pie(data, options);
</script>
</div>
@stop