@extends('admin.layout')
@section('content')
	<div class="showback">
	                          <span style = "font-size:1.5em;color: #00549F">List of Events</span><hr><table class="table table-striped table-advance table-hover">
	                  	  	  

	                          <thead>
	                          	<tr>
	                          		<th>#</th>
	                          		<th>Event Title</th>
	                          		<th>Event Date</th>
	                          		<th>Event Description</th>
	                          		<th>Action</th>
	                          	</tr>
	                          </thead>
	                          <tbody>
	                          	@foreach($events as $event)
	                          	<tr>
	                          		<td>{{$event->id}}</td>
	                          		<td>{{$event->event_title}}</td>
	                          		<td>{{$event->event_date}}</td>
	                          		<td>{{substr($event->event_description,0,35)}}...</td>
	                          		<td>
	                          			<div class="btn-group">
                                        <button type="button" class="btn btn-theme03 btn-xs dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                          <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a data-toggle="modal" data-target="#eventModal{{$event->id}}">Delete Event</a></li>
                                          <li><a  data-toggle="modal" data-target="#updateModal{{$event->id}}">Update Event</a></li>
                                          
                                        </ul>
                                      </div>

	                          			
								                  	<div class="modal fade" id="eventModal{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
													  <div class="modal-dialog">
													    <div class="modal-content">
													      <div class="modal-header">
													        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													        <h4 class="modal-title" id="myModalLabel">Delete</h4>
													      </div>
													      <div class="modal-body">
													        Are you sure you want to delete the event: "{{$event->event_title}}"?
													      </div>
													      <div class="modal-footer">
													        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													        <a class = "btn btn-success" href="/deleteevent/{{$event->id}}">Delete</a>
													        </div>
													    </div>
													  </div>
													</div>

													<!-- <center>
													<button class="btn btn-success" data-toggle="modal" data-target="#eventModal{{$event->id}}">
													  Delete Event
													</button>
													</center> -->

													<div class="modal fade" id="updateModal{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
													  <div class="modal-dialog">
													    <div class="modal-content">
													    <form action="updateEvent" method="post">
													      <div class="modal-header">
													        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													        <h4 class="modal-title" id="myModalLabel">Update Event Details</h4>
													      </div>
													      <div class="modal-body">
													      <input type="hidden" value="{{$event->id}}" name="event_id">
													      <label>Event Name</label><input class="form-control" value="{{$event->event_title}}" name="event_title" required/>
													      <label>Event Date</label><input type="date" class="form-control" value="{{$event->event_date}}" name="event_date" required/>
													      <label>Event Description</label><textarea class="form-control" name="event_description" required>{{$event->event_description}}</textarea>
													      </div>
													      <div class="modal-footer">
													        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													        <input class="btn btn-primary" type="submit" value="Update">
													      </div>
													    </form>
													    </div>
													  </div>
													</div>

													<!-- <center>
													<button class="btn btn-success" data-toggle="modal" data-target="#updateModal{{$event->id}}">
													  Update Event Details
													</button>
													</center> -->
	                          		</td>
	                          	</tr>
	                          	@endforeach
	                          </tbody>

	                      </table>
	                  </div>

@stop