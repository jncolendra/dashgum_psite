@extends('admin.layout')
@section('content')
<?php
$userArchive = User::onlyTrashed()->get();
?>
	<div class="showback">
		<span style = "font-size:1.5em;color: #00549F"> Archived Members</span><hr>
		<table class="table table-striped table-advance table-hover">
		 	<thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID:</th>
                                  <th><i class="fa fa-user"></i> Username:</th>
                                  <th> Membership Type:</th>
                                  <th> Membership Status:</th>
                                  <th> Commands:</th>
                              </tr>
            </thead>
            <tr>
            	@foreach($userArchive as $result)
	            	<td>
	            	{{$result->membership_id}}
	            	</td>
	            	<td>
	            	{{$result->username}}
	            	</td>
	            	<td>
	            	{{$result->usertype}}
	            	</td>
	            	<td>
	            	Inactive + Archived
	            	</td>
            	@endforeach
            </tr>
         </table>
	</div>

@stop