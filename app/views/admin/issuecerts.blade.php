@extends('admin.layout')
@section('content')
<?php
$users = DB::table('users')->get();
?>
<div class="col-md-12">
                      <div class="content-panel">
                         <span style = "font-size:1.5em;color: #00549F"> List of Certificate Members</span><hr><table class="table table-striped table-advance table-hover">
                            
                            
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i> E-Mail</th>
                                  <th><i class="fa fa-calendar"></i> Member Since:</th>
                                  <th> Approval Status: </th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                    	{{$user->membership_id}}
                                    </td>
                                    <td>
                                    	{{$user->username}}
                                    </td>
                                    <td>
                                    	{{$user->email}}
                                    </td>
                                    <td>
                                    	{{$user->created_at}}
                                    </td>
                                    <td>
                                    	{{$user->approval}}
                                    </td>

                                    <!-- DB Controls -->
                                   
                                  <td>

	<a href="/certview/{{$user->id}}" target="_blank">View Certificate</a>
                                     
                                      <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                                  </td>
                               
                                </tr>
                              @endforeach
                             
                              
                              </tbody>
                          </table>
                          
                      </div><!-- /content-panel -->
                      </div>
@stop