@extends('admin.layout')
@section('content')
<?php
$renewals = DB::table('renewals')->get();
$users = DB::table('users')->get();
?>
<div class="col-md-12" id="form">
  <div class="showback">
   <span style = "font-size:1.5em;color: #00549F">Statement of Accounts: Renewal</span>
                            
    <form class="form-group" action="submitRenewal" method="POST">
      <p>
      Select User to Renew:
      <p>
      <select class="form-control" name="user_id">
         @foreach ($users as $user)
                <option value="{{$user->id}}">{{$user->username}}</option>
         @endforeach
      </select>
      <p>
      Enter Amount of Payment Made:
      </p>
      <!-- <input type="number" class="form-control" placeholder="0.00" name="payment_amount" required/> -->
      <input type="submit" class="btn btn-primary btn-block">
    </form>
    <hr>
    <br>
    <button id="btnform" class="btn btn-success btn-block"> View Report </button>
  </div>
</div>
<div class="col-md-12" id="table" style="display: none;">
                      <div class="showback">
                          <h4><i class="fa fa-angle-right"></i> Statement of Accounts: Renewal</h4><hr><table class="table table-striped table-advance table-hover">
                            
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Transaction ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i>Next Renewal Date:</th>
                                  
                                  <th> Payment </th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($renewals as $renewal)
                                <tr>
                                    <td>
                                    	{{$renewal->id}}
                                    </td>
                                    <td>
                                    	{{DB::table('users')->where('id',$renewal->user_id)->pluck('username');}}
                                    </td>
                                   
                                    <td>
                                    	{{$renewal->renewal_date}}
                                    </td>
                                    <td>
                                    	{{$renewal->payment_amount}}
                                    </td>

                                    <!-- DB Controls -->
                                   
                                  <td>

	
                                     
                                      <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                                  </td>
                               
                                </tr>
                              @endforeach
                             <tr>
                             	<td></td>
                             	<td></td>
                             	<td><b>Total:</b></td>

                             	<td>
                              Php {{DB::table('renewals')->sum('payment_amount')}}
                             	</td>
                             	<td></td>
                             </tr>
                              
                              </tbody>
                          </table>
                          <button id="btntable" class="btn btn-primary btn-success btn-block"> Back to Controls </button>
                      </div><!-- /content-panel -->
                      </div>
<script type="text/javascript">
  $('#btnform').click(function(){
    $('#table').toggle("slow");
    $('#form').toggle("slow");
  });
  $('#btntable').click(function(){
    $('#table').toggle("slow");
    $('#form').toggle("slow");
  });
</script>
@stop