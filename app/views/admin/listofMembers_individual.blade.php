@extends('admin.layout')
@section('content')
<div class="showback">
	                            <span style = "font-size:1.5em;color: #00549F"> List of Individual Members</span><hr><table class="table table-striped table-advance table-hover">
	                  	  	  
	                  	  	  
	                              <thead>
	                              <tr>
	                                  <th>#</th>
	                                  <th>Username</th>
	                                  <th>First Name</th>
	                                  <th>Last Name</th>
	                                  <th>Educational Attainment</th>
	                                  <th>Institution Affiliation</th>
	                                  <th>Institution President</th>
	                                  <th>E-Mail</th>
	                                  <th></th>
	                              </tr>
	                              </thead>
	                              <tbody>
	                              @foreach($users as $user)
	                              <tr>
	                              	  <td>{{$user->membership_id}}</td>	
	                                  <td>{{$user->username}}</td>
	                                  <td>{{$user->individual_fname}}</td>
	                                  <td>{{$user->individual_lname}}</td>
	                                  <td>{{$user->individual_attainment}}</td>
	                                  <td>{{$user->individual_affiliation}}</td>
	                                  <td>{{$user->individual_president}}</td>
	                                  <td>{{$user->email}}</td>
	                                  <td><a href="/profile/{{$user->username}}">View Profile</a></td>
	                              </tr>
	                              @endforeach
	                              </tbody>
	                          </table>
	                  	  </div>
@stop