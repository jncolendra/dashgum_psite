@extends('admin.layout')
@section('content')
<!-- Table  -->
                      <div class="col-md-12">
                      <div class="showback">
                       <span style = "font-size:1.5em;color: #00549F"> Approval of Membership</span><hr><table class="table table-striped table-advance table-hover">
                            
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i> E-Mail</th>
                                  <th><i class="fa fa-calendar"></i> Member Since:</th>
                                  <th> Approval Status: </th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                      {{$user->membership_id}}
                                    </td>
                                <td>
                                  {{$user->username}}
                                                </td>
                                <td>
                                  {{$user->email}}
                                                </td>
                                <td>
                                  {{$user->created_at}}
                                                </td>
                                                <td>
                                  {{$user->approval}}
                                                </td>

                                    <!-- DB Controls -->
                                   
                                  <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-theme03 btn-xs dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                          <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a data-toggle="modal" data-target="#approveModal{{$user->id}}">Approve User</a></li>
                                          <li><a data-toggle="modal" data-target="#paymentModal{{$user->id}}">Settle Payment</a></li>
                                          
                                        </ul>
                                      </div>
                                    <div class="modal fade" id="approveModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="myModalLabel">Approval of Users</h4>
                                </div>
                                <div class="modal-body">
                                  Are you sure you want to approve the user: "{{$user->username}}"?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <a href="/approveuser/{{$user->id}}"><button class="btn btn-success"><i class="fa fa-check"></i> Approve User</button></a>
                                  </div>
                              </div>
                            </div>
                          </div>

                          <center>
                          <!-- <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#approveModal{{$user->id}}">
                           Approve User
                          </button> -->
                          <div class="modal fade" id="paymentModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <form method="post" action="/settlepayment">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="myModalLabel">Payment</h4>
                                </div>
                                <div class="modal-body">
                                  <h5>Payment Made : {{$user->payment}}</h5>
                                  <a href="/payment_photo/{{$user->payment_photo}}" target="_blank">Uploaded Photo Here</a><br>
                                  <label>Enter Amount of Payment:</label><!-- <input type="number" class="form-control" placeholder="0.00" name="settlePayment" required> -->
                                  <select id="membership_type" class="form-control" required name="settlePayment">
                                    <option value="" default select>--Select Membership Type--</option>
                                    <option value="500">Individual</option>
                                    <option value="3000">Institutional</option>
                                    <option value="10000">Corporate</option>
                                  </select>
                                  <input type="hidden" value="{{$user->id}}" name="userID">
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <input type="submit" value="Settle Payment" class="btn btn-primary">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          </center>
                          <!-- <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#paymentModal{{$user->id}}">
                           Settle Payment
                          </button> -->
                                     
                                      <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                                  </td>
                               
                                </tr>
                              @endforeach
                             
                              
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                      </div>
@stop