@extends('admin.layout')
@section('content')
<?php
$users = DB::table('users')->get();
?>
<div class="col-md-12">
                      <div class="content-panel">
                        <span style = "font-size:1.5em;color: #00549F"> Statement of Accounts: Registration</span><hr><table class="table table-striped table-advance table-hover">
                            
                            
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i> E-Mail</th>
                                  <th><i class="fa fa-calendar"></i> Member Since:</th>
                                  <th> Payment </th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                    	{{$user->membership_id}}
                                    </td>
                                    <td>
                                    	{{$user->username}}
                                    </td>
                                    <td>
                                    	{{$user->email}}
                                    </td>
                                    <td>
                                    	{{$user->created_at}}
                                    </td>
                                    <td>
                                    	{{$user->payment}}
                                    </td>

                                    <!-- DB Controls -->
                                   
                                  <td>

	
                                     
                                      <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                                  </td>
                               
                                </tr>
                              @endforeach
                             <tr>
                             	<td></td>
                             	<td></td>
                             	<td></td>
                             	<td><b>Total:</b></td>

                             	<td>
                             	Php	{{DB::table('users')->sum('payment')}}
                             	</td>
                             	<td></td>
                             </tr>
                              
                              </tbody>
                          </table>
                          
                      </div><!-- /content-panel -->
                      </div>


@stop