@extends('admin.layout')
@section('content')
<div class="showback">
	                          <span style = "font-size:1.5em;color: #00549F"> List of Corporate Members</span><hr><table class="table table-striped table-advance table-hover">
	                  	  	  
	                  	  	  
	                  	  	  
	                              <thead>
	                              <tr>
	                                  <th>#</th>
									  <th>Company Name</th>
									  <th>Company Address</th>
									  <th>Company Representative</th>
									  <th>Company Contact</th>
									  <th>E-Mail</tH>
	                                  <th></th>
	                              </tr>
	                              </thead>
	                              <tbody>
	                              @foreach($users as $user)
	                              <tr>
	                              	  <td>{{$user->membership_id}}</td>	
	                                  <td>{{$user->corporate_companyname}}</td>
	                                  <td>{{$user->corporate_address}}</td>
	                                  <td>{{$user->corporate_rep}}</td>
	                                  <td>{{$user->corporate_rep_contact}}</td>
	                                  <td>{{$user->email}}</td>
	                                  <td><a href="/profile/{{$user->username}}">View Profile</a></td>
	                              </tr>
	                              @endforeach
	                              </tbody>
	                          </table>
	                  	  </div>
@stop