@extends('admin.layout')
@section('content')
<?php 
$total = DB::table('users')->sum('payment');
$totalrenewal = DB::table('renewals')->sum('payment_amount');
?>
{{HTML::script("/js/chart-master/Chart.js")}}

<div class="showback">
	<span style = "font-size:1.5em;color: #00549F"> Accounting</span>
	                  	  	  
	<div class="alert alert-info">
		<div style="float:right;">
			<center>
			<canvas id="myChart"></canvas><br>
			<b>Pie Chart of Collected Fees<b>
			</center>
		</div>
	

	<h4>Reports on the Fees Collected:</h4><br>
	
	<b>Total Fees Collected via Membership:</b> <h2><span>Php {{$total}}</span></h2>
	<br>
	<b>Total Fees Collected via Renewal:</b> <h2><span>Php {{$totalrenewal}}</span></h2>
	<a href="/payReg"><button type="button" class="btn btn-primary btn-lg btn-block">View Accounting Statements: Registration Fees</button></a>
	<center>-Or-</center>
	<a href="/payRen"><button type="button" class="btn btn-success btn-lg btn-block">View Accounting Statements: Renewal Fees</button></a>
	<hr>
	The Pie chart on the right can be saved as an image for various uses.
	</div>
<script>
var data = [
    {
        value: {{$total}},
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Membership Fees"
    },
    {
        value: {{$totalrenewal}},
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Renewal Fees"
    }
]
var ctx = document.getElementById("myChart").getContext("2d");
var myNewChart = new Chart(ctx).Pie(data);
new Chart(ctx).Pie(data, options);
</script>
</div>
@stop