<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   

    <title>PSITE - Administration Portal</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('/css/bootstrap.css'); }}
    
    <!--external css-->
    {{ HTML::style('/font-awesome/css/font-awesome.css'); }}
    {{ HTML::style('/lineicons/style.css'); }}     
    <!-- Custom styles for this template -->
    {{ HTML::style('/css/style.css'); }}
    {{ HTML::style('/css/style-responsive.css'); }}

    <!-- JS -->
    {{ HTML::script('/js/jquery.js'); }}
    {{ HTML::script('/js/bootstrap.min.js'); }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="getOnline();">

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/admin" class="logo"><b>Philippine Society of Information Technology Educators</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="users/logout"><i class="fa fa-times fa-lg"></i> Logout</a></li>
              </ul>
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="/dashboard"><i class="fa fa-rocket fa-lg"></i> Dashboard</a></li>
              </ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered">
                    <img src="profilephotos/{{Confide::user()->photofilename}}" class="img-circle" height= "60"width="60" id="btnphoto">
                    <form id="photoform" action="/uploadPhoto" method="POST" enctype="multipart/form-data">
                      <input id="uploadphoto" type="file" name="file" capture="camera" style="display: none;">
                    </form>
                  </p>
                  <h5 class="centered">{{Confide::user()->username ?: 'visitor'}}</h5>
                  <li class="mt">
                      <a href="/admin">
                          <i class="fa fa-dashboard"></i>
                          <span>Control Panel</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Accounts</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/approvalofmembers">Approval of Membership</a></li>
                          <li><a  href="/registeredusers">Registered Users</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-group"></i>
                          <span>List of Members </span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/membersreport">Reports</a></li>
                          <li><a  href="/ActiveMembers">Active and Inactive Members</a></li>
                          <li><a  href="/listofmembers_individual">Individual</a></li>
                          <li><a  href="/listofmembers_institutional">Institution</a></li>
                          <li><a  href="/listofmembers_corporate">Corporate</a></li>                         
                          <li><a  href="/issuecerts">E-Certificate</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="/accounting" >
                          <i class="fa fa-dollar"></i>
                          <span> Accounting</span>
                      </a>
                      
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-star"></i>
                          <span>Events</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/createannouncement">Create an Event</a></li>
                          <li><a  href="/eventAttendance">Event Attendance</a></li>
                          <li><a  href="/listofevents">List of Events</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-asterisk"></i>
                          <span>Archive</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/archive">Members</a></li>
                          <li><a  href="/archiveEvents">Events</a></li>
                      </ul>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      @if(Session::get("notice"))
      <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Notice</h4>
            </div>
            <div class="modal-body">
             {{Session::get("notice")}}
            </div>
            <div class="modal-footer">
              <button id="btnSession" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">
      $(window).load(function(){
        $("#paymentModal").modal("show");
      });
      $("#btnSession").click(function(){
        $.ajax({
          url:"/removeSession",
          type:"get"
        });
      });
      $('#paymentModal').on('hidden.bs.modal', function () {
        $.ajax({
          url:"/removeSession",
          type:"get"
        });
      });
      </script>
      @endif
      <section id="main-content">
          <section class="wrapper">
            <div class="row">
              <div class="col-lg-9 main-chart" id="printableArea">
              @yield('content')
              </div>

              <div class="col-lg-3 ds">
              <h3><span id="notifcount" class="badge bg-theme"><i class="fa fa-spinner fa-spin"></i></span>NOTIFICATIONS</h3>
              
              <br>
                <div id="notification" style="height: 160px; overflow-y: scroll;">
                <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center>   
                </div>
              
              <h3>Who's Online</h3>
              <br>
                <div id="online" style="height: 198px; overflow-y: scroll;">
                  <center><br><i class="fa fa-spinner fa-spin"></i><br><br>Loading...</center>
                </div>
                          
              </div>
            </div>
            
          
          </section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Capstone Project 2015 - Trinity University of Asia by Colendra, Villareal, Marticio, Soliman
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    {{ HTML::script('/js/jquery-ui-1.9.2.custom.min.js'); }}
    {{ HTML::script('/js/jquery.ui.touch-punch.min.js'); }}
    {{ HTML::script('/js/jquery.dcjqaccordion.2.7.js'); }}
    {{ HTML::script('/js/jquery.scrollTo.min.js'); }}
    {{ HTML::script('/js/jquery.nicescroll.js'); }}

    <!--common script for all pages-->
    {{ HTML::script('/js/common-scripts.js'); }}

    <!--script for this page-->
  <script>
      $(window).on('load', function(){
      $.ajax("/active");
      });
      $(window).on('unload', function(){
      $.ajax("/idle");
      });
      setInterval(function(){
        $.ajax({
          type:"get",
          url:"/notifcount",
          timeout: 5000,
          success : function(count){
            $("#notifcount").html(count);
            $("#notification").load("/getnotif");
          }
        });
      },1000);
      //custom select box
      $(function(){
          $('select.styled').customSelect();
      });

      setInterval(function(){getOnline()},5000); //Get Online Users Every 30secs
      /*Upload Photo jQuery*/
      $('#btnphoto').click(function(){
          $('#uploadphoto').click();
        });
      $('#uploadphoto').change(function(){
        $('#photoform').submit();
      });

      /*Who Is Online - Ajax*/
      function getOnline(){
            $.ajax({
              type: 'GET',
              url: '/getonline',
              success:function(online){
                $('#online').html('');
                  for(x in online){
                      $('#online').append(
                        "<div class='desc'><div class='thumb'><img class='img-circle' width='35' height='35' src='/profilephotos/"+online[x].photofilename+"'></div><div class='details'><p><a href='/profile/"+online[x].username+"'>"+online[x].username+"</a></br><muted>AVAILABLE</muted></p></div></div>"
                        );
                  }
              }
            });
      }
  </script>
  
  </body>
</html>
