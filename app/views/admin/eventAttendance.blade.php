@extends('admin.layout')
@section('content')
<?php 
$events = DB::table('events')->get();
$users = DB::table('users')->get();
?>
<div class="showback">
	<span style = "font-size:1.5em;color: #00549F"> Event Attendance</span>
	                  	  	  
	<a href="eventListAttendance" class="btn btn-success btn-block">View List of Event Attendance</a>
	<center><b>-Or-</b></center>
	<form action="/submitAttendance" method="post">
		Event Name:
		<select class="form-control" name="event_id" id="event" required>
						  <option value="" selected id="disablethis">--Select an Event--</option>
						  @foreach ($events as $event)
						  <option value="{{$event->id}}">{{$event->event_title}}</option>
						  @endforeach
		</select><br>
		<div>
			<div class="col-lg-6">
			<h4>Event Attendee/s: </h4>
			(Multiple Select: Ctrl+Click):
			<select multiple class="form-control" id="attendee" name="attendees[]" size="6" required>
							  @foreach ($users as $user)
							  <option value="{{$user->id}}">{{$user->username}}</option>
							  @endforeach
			</select>
			</div>
			<div class="col-lg-6">
				<h4>Users to Add:</h4>
				<br>
				<h5><b id="eventName"></b></h5>
				<div id="selectedUsers" style="overflow-y: scroll; height: 120px;">
				</div>
			</div>
		</div>
		<input type="submit" class="btn btn-primary btn-block" value="Submit Attendance">
	</form>
	
</div>
<script>
	$("#attendee").change(function() {
	    var str = " ";
	    $("#attendee option:selected").each(function() {
	      str += $( this ).text() + "<br>";
	    });
	    $("#selectedUsers").html( str );
  	})
  .trigger( "change" );
  $("#event").change(function(){
	  	var str = " ";
	  	$("#event option:selected").each(function() {
		      str += $( this ).text() + "<br>";
		});
		$("#eventName").html(str);
  });
  $("#event").click(function(){
  	$("#disablethis").prop("disabled",true);
  });
</script>
@stop