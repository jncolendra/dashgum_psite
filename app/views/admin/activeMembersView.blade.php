@extends('admin.layout')
@section('content')
<?php
$users = User::all();
?>
	<div class="showback">
		   <span style = "font-size:1.5em;color: #00549F"> Membership Status</span><hr><table class="table table-striped table-advance table-hover">
		 	<thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID:</th>
                                  <th><i class="fa fa-user"></i> Username:</th>
                                  <th> Membership Type:</th>
                                  <th> Membership Status:</th>
                                  <th> Commands:</th>
                              </tr>
            </thead>
		@foreach($users as $user)
		<tr>
			<?php
			$active = DB::table('renewals')->where('user_id',$user->id)->first();
			?>
			@if($active)
				<td>{{DB::table('users')->where('id',$active->user_id)->pluck('membership_id')}}</td>
				<td>{{DB::table('users')->where('id',$active->user_id)->pluck('username')}}</td>
				<td>{{DB::table('users')->where('id',$active->user_id)->pluck('usertype')}}</td>
				<td>
					Active
				</td>
				<td></td>
			@else
				<?php
				$count = DB::table('renewals')->where('user_id',$user->id)->count();
				?>
				@if($count < 5)
					<?php 
						$date1 = date_create($user->first_renewal_date);
						$date2 = date_create(date("Y-m-d"));
						$interval = date_diff($date1, $date2);
					?>
					@if($interval->y <	5)
						<td>{{$user->membership_id}}</td>
						<td>{{$user->username}}</td>
						<td>{{$user->usertype}}</td>
						<td>Active</td>
						<td></td>
					@else
						<td>{{$user->membership_id}}</td>
						<td>{{$user->username}}</td>
						<td>{{$user->usertype}}</td>
						<td>Inactive</td>
						<td>
							<a data-toggle="modal" class="btn btn-primary" data-target="#deleteModal{{$user->id}}">Delete User</a>
						</td>
						<div class="modal fade" id="deleteModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="myModalLabel">User Deletion</h4>
                                </div>
                                <div class="modal-body">
                                  Are you sure you want to delete the user: "{{$user->username}}"?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <a href="/deleteInactive/{{$user->id}}"><button class="btn btn-success"><i class="fa fa-check"></i> Delete User</button></a>
                                  </div>
                              </div>
                            </div>
                          </div>
					@endif
				@endif
			@endif
		</tr>
		@endforeach
	</table>
	</div>	
@stop