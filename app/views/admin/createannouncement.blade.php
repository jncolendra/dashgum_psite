@extends('admin.layout')
@section('content')
	
	<div class="showback">
		<span style = "font-size:1.5em;color: #00549F"> Create Event</span>
                            
		<form class="form-horizontal style-form" method="post" enctype="multipart/form-data" action="/createEvent">
      						          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Title of Event</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="event_title" required>
                                  <span class="help-block">Enter the title of the Event to be Created.</span>
                              </div>
                          	</div>
                          	<div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Date of Event</label>
                              <div class="col-sm-10">
                      <input type="date" class="form-control" name="event_date" min="{{date('Y-m-d')}}" required>
                                  <span class="help-block">Enter the Date of the Event to be Created.</span>
                              </div>
                          	</div>
                          	<div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Place of Event</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="event_place" required>
                                  <span class="help-block">Enter the place of the Event to be Created.</span>
                              </div>
                          	</div>
                            <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Description of Event</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" rows="5" name="event_description" required></textarea>
                                  <span class="help-block">Enter the Description of the Event to be created, this will show up in the Dashboard Feed.</span>
                              </div>
                            </div>
              							<div class="form-group">
              									<label class="col-sm-2 col-sm-2 control-label">Picture of Event</label>
              		                  <div class="col-sm-10">
              											<input id="uploadphoto" type="file" name="file" accept="image/png, image/jpeg" capture="camera"/>
              											<span class="help-block">Uploads a picture of an Event which will be used as a banner.</span>
              									</div>
              							</div>
                            <div class="form-group">
                                <div class="col-sm-5 col-sm-5 control-label"></div>
                                <div class="col-sm-5">
                                <input type="submit" value="Post" class="btn btn-primary btn-lg"/>
                                </div>
                                    
                            </div>
                          	<hr>
                          	<p> The Event you will create here will be visible to the Users in the social-side of the app. </p>
        </form>        
    </div>

@stop