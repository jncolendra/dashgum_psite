@extends('admin.layout')
@section('content')

                  <?php 
                  $newest = DB::table('users')->orderBy('created_at', 'desc')->first();
                  ?>
    						        <div class="col-lg-4 col-md-4 col-sm-4 mb">
                            <div class="white-panel pn">
                              <div class="white-header">
                                <h5>NEWEST MEMBER</h5>
                              </div>
                              <p><img src="profilephotos/{{$newest->photofilename}}" class="img-circle" width="50"></p>
                              <p><b>{{$newest->username}}</b></p>
                                <div class="row">
                                  <div class="col-md-6">
                                    <p class="small mt">MEMBERSHIP TYPE</p>
                                    <p>{{$newest->usertype}}</p>
                                  </div>
                                  <div class="col-md-6">
                                    <p class="small mt">MEMBERSHIP SINCE:</p>
                                    <p>{{$newest->created_at}}</p>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 mb">
                                  <div class="darkblue-panel pn">
                                    <div class="darkblue-header">
                                        <h5>Registered Users</h5>
                                                </div>
                                                <h1 class="mt"><i class="fa fa-user fa-3x"></i></h1>
                                    <p>+ {{ User::where('confirmed','0')->count();}} NEW USER</p>
                                    <footer>
                                      <div class="centered">
                                        <h5><i class="fa fa-user"></i> {{ User::where('confirmed','1')->count();}} Approved User/s</h5>
                                      </div>
                                    </footer>
                                  </div><!--  /darkblue panel -->
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 mb">
                        <div class="twitter-panel pn">
                          <i class="fa fa-twitter fa-4x"></i>
                          <p>PSITE is here! Take a look and enjoy this new Dashboard System.</p>
                          <h4 class="user"><a href="https://twitter.com/1psitencr">@1PSITEncr</a></h4>
                        </div>
                      </div>

                      <!-- Table  -->
                      <div class="col-md-12">
                      <div class="content-panel">
                          <h4><i class="fa fa-angle-right"></i> Approval of Membership</h4><hr><table class="table table-striped table-advance table-hover">
                            
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-asterisk"></i>Membership ID</th>
                                  <th><i class="fa fa-user"></i> Username</th>
                                  <th><i class="fa fa-envelope"></i> E-Mail</th>
                                  <th><i class="fa fa-calendar"></i> Member Since:</th>
                                  <th> Approval Status: </th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                      {{$user->membership_id}}
                                    </td>
                                <td>
                                  {{$user->username}}
                                                </td>
                                <td>
                                  {{$user->email}}
                                                </td>
                                <td>
                                  {{$user->created_at}}
                                                </td>
                                                <td>
                                  {{$user->approval}}
                                                </td>

                                    <!-- DB Controls -->
                                    <td>
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-theme03 btn-xs dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                          <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a data-toggle="modal" data-target="#approveModal{{$user->id}}">Approve User</a></li>
                                          <li><a data-toggle="modal" data-target="#paymentModal{{$user->id}}">Settle Payment</a></li>
                                          
                                        </ul>
                                      </div>
                                      <div class="modal fade" id="approveModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                </div>
                                <div class="modal-body">
                                  Are you sure you want to approve the user: "{{$user->username}}"?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <a href="/approveuser/{{$user->id}}"><button class="btn btn-success"><i class="fa fa-check"></i> Approve User</button></a>
                                  </div>
                              </div>
                            </div>
                          </div>

                          <center>
                          <!-- <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#approveModal{{$user->id}}">
                           Approve User
                          </button> -->
                          <div class="modal fade" id="paymentModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <form method="post" action="/settlepayment">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="myModalLabel">Payment</h4>
                                </div>
                                <div class="modal-body">
                                  <h5>Payment Made : {{$user->payment}}</h5>
                                  <a href="/payment_photo/{{$user->payment_photo}}" target="_blank">Uploaded Photo Here</a><br>
                                  <label>Enter Amount of Payment:</label><!-- <input type="number" class="form-control" placeholder="0.00" name="settlePayment" required> -->
                                   <select id="membership_type" class="form-control" required name="settlePayment">
                                    <option value="" default select>--Select Membership Type--</option>
                                    <option value="500">Individual</option>
                                    <option value="3000">Institutional</option>
                                    <option value="10000">Corporate</option>
                                  </select>
                                  <input type="hidden" value="{{$user->id}}" name="userID">
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <input type="submit" value="Settle Payment" class="btn btn-primary">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <center>
                          <!-- <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#paymentModal{{$user->id}}">
                           Settle Payment
                          </button> -->
                                      <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                                  </td>
                                </tr>
                              @endforeach
                             
                              
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                      </div>

@stop