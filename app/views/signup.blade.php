@extends('landingpage.loginlayout')
@section('content')
<form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" class="form-login" style="max-width: 90% !important;">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
     <h2 class="form-login-heading">Sign Up Forms</h2>
     <div class="login-wrap">
    <fieldset>
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif

        @if (Session::get('notice'))
            <div class="alert">{{ Session::get('notice') }}</div>
        @endif

        <div class="form-group">
            <label>Membership Type</label>
                <select id="membership_type" class="form-control" required name="usertype">
                  <option value="" default select>--Select Membership Type--</option>
                  <option value="individual">Individual - Php 500.00</option>
                  <option value="institutional">Institutional - Php 3000.00</option>
                  <option value="corporate">Corporate - Php 10,000.00</option>
                </select>           
        </div>
        @if (Cache::remember('username_in_confide', 5, function() {
            return Schema::hasColumn(Config::get('auth.table'), 'username');
        }))
            <div class="form-group">
                <label for="username">{{{ Lang::get('confide::confide.username') }}}</label>
                <input class="form-control" placeholder="{{{ Lang::get('confide::confide.username') }}}" type="text" name="username" id="username" value="{{{ Input::old('username') }}}">
            </div>
        @endif
        <div class="form-group">
            <label for="email">{{{ Lang::get('confide::confide.e_mail') }}} <small>{{ Lang::get('confide::confide.signup.confirmation_required') }}</small></label>
            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
        </div>
        <div class="form-group">
            <label for="password">{{{ Lang::get('confide::confide.password') }}}</label>
            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
        </div>
        <div class="form-group">
            <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
        </div>
		
        <div class="form-group">
			<label>Org Position</label>
				<select class="form-control" required name="officer_position">
				  <option value="member">Member</option>
				  <option value="officer">Officer</option>
				  
				</select>			
		</div>

        <!-- Individual -->

        <div class="form-group col-xs-6" id="individual_fname" style="display: none;">
            <label for="individual_fname">First Name</label>
            <input class="form-control" placeholder="First Name" type="text" name="individual_fname">
        </div>

        <div class="form-group col-xs-6" id="individual_lname" style="display: none;">
            <label for="individual_lname">Last Name</label>
            <input class="form-control" placeholder="Last Name" type="text" name="individual_lname">
        </div>

        <div class="form-group col-xs-6" id="individual_affiliation" style="display: none;">
            <label for="individual_affiliation">Institution Affiliation</label>
            <input class="form-control" placeholder="Institution Affiliation" type="text" name="individual_affiliation">
        </div>

        <div class="form-group col-xs-6" id="individual_president" style="display: none;">
            <label for="individual_president">Institution President</label>
            <input class="form-control" placeholder="Institution President" type="text" name="individual_president">
        </div>

        <div class="form-group col-xs-12" id="individual_attainment" style="display: none;">
            <label for="individual_attainment">Highest Educational Attainment</label>
            <input class="form-control" placeholder="Highest Educational Attainment" type="text" name="individual_attainment">
        </div>

        <!-- Instiutional -->

        <div class="form-group col-xs-4" id="institution_affiliation" style="display: none;">
            <label for="institution_affiliation">Institution Affiliation</label>
            <input class="form-control" placeholder="Institution Affiliation" type="text" name="institution_affiliation">
        </div>

        <div class="form-group col-xs-4" id="institution_address" style="display: none;">
            <label for="institution_address">Institution Address</label>
            <input class="form-control" placeholder="Institution Address" type="text" name="institution_address">
        </div>

        <div class="form-group col-xs-4" id="institution_president" style="display: none;">
            <label for="institution_president">Institution President</label>
            <input class="form-control" placeholder="Institution President" type="text" name="institution_president">
        </div>

        <div class="form-group col-xs-6" id="institution_rep1_name" style="display: none;">
            <label for="institution_rep1_name">#1 Representative Name</label>
            <input class="form-control" placeholder="Representative Name" type="text" name="institution_rep1_name">
        </div>
        <div class="form-group col-xs-6" id="institution_rep2_name" style="display: none;">
            <label for="institution_rep2_name">#2 Representative Name</label>
            <input class="form-control" placeholder="Representative Name" type="text" name="institution_rep2_name">
        </div>

        <div class="form-group col-xs-6" id="institution_rep1_contact" style="display: none;">
            <label for="institution_rep1_contact">#1 Representative Contact</label>
            <input class="form-control" placeholder="Representative Name" type="text" name="institution_rep1_contact">
        </div>
        <div class="form-group col-xs-6" id="institution_rep2_contact" style="display: none;">
            <label for="institution_rep2_contact">#2 Representative Contact</label>
            <input class="form-control" placeholder="Representative Name" type="text" name="institution_rep2_contact">
        </div>
        
        <!-- Corporate -->
        <div class="form-group col-xs-6" id="corporate_companyname" style="display: none;">
            <label for="corporate_companyname">Company Name</label>
            <input class="form-control" placeholder="Company Name" type="text" name="corporate_companyname">
        </div>

        <div class="form-group col-xs-6" id="corporate_address" style="display: none;">
            <label for="corporate_address">Company Address</label>
            <input class="form-control" placeholder="Company Address" type="text" name="corporate_address">
        </div>
        <div class="form-group col-xs-6" id="corporate_rep" style="display: none;">
            <label for="corporate_rep">Company's Representative Name</label>
            <input class="form-control" placeholder="Company's Representative Name" type="text" name="corporate_rep">
        </div>
        <div class="form-group col-xs-6" id="corporate_rep_contact" style="display: none;">
            <label for="corporate_rep_contact">Company's Representative Contact</label>
            <input class="form-control" placeholder="Company's Representative Contact" type="text" name="corporate_rep_contact">
        </div>

        <div class="form-actions form-group" style="float: right;">
          <button type="submit" class="btn btn-theme"><i class="fa fa-check"></i> {{{ Lang::get('confide::confide.signup.submit') }}}</button>
        </div>
		
		<div class="registration">
		               
		</div>
    </fieldset>
</div>
</form>

<script>
$('#membership_type').change(function(){
    var x = $(this).find('option:selected').attr('value');
    if (x == "individual"){
        /*Show Elements*/
        $('#individual_fname').show("slow");
        $('#individual_lname').show("slow");
        $('#individual_affiliation').show("slow");
        $('#individual_president').show("slow");
        $('#individual_attainment').show("slow");

        /*Dynamic Adding of Required Attributes*/
        $("input[name='individual_fname']").prop('required', true);
        $("input[name='individual_lname']").prop('required', true);
        $("input[name='individual_affiliation']").prop('required', true);
        $("input[name='individual_president']").prop('required', true);
        $("input[name='individual_attainment']").prop('required', true);

        /*Dynamic Removing of Required Attributes*/
        $("input[name='institution_affiliation']").prop('required', false);
        $("input[name='institution_address']").prop('required', false);
        $("input[name='institution_president']").prop('required', false);
        $("input[name='institution_rep1_name']").prop('required', false);
        $("input[name='institution_rep2_name']").prop('required', false);
        $("input[name='institution_rep1_contact']").prop('required', false);
        $("input[name='institution_rep2_contact']").prop('required', false);
        $("input[name='corporate_companyname']").prop('required', false);
        $("input[name='corporate_address']").prop('required', false);
        $("input[name='corporate_rep']").prop('required', false);
        $("input[name='corporate_rep_contact']").prop('required', false);

        /*Hide Other Elements*/
        $("#institution_affiliation").hide("slow");
        $("#institution_address").hide("slow");
        $("#institution_president").hide("slow");
        $("#institution_rep1_name").hide("slow");
        $("#institution_rep2_name").hide("slow");
        $("#institution_rep1_contact").hide("slow");
        $("#institution_rep2_contact").hide("slow");
        $('#corporate_companyname').hide("slow");
        $('#corporate_address').hide("slow");
        $('#corporate_rep').hide("slow");
        $('#corporate_rep_contact').hide("slow");

        /*Clear Values*/
        $("input[name='institution_affiliation']").val("");;
        $("input[name='institution_address']").val("");;
        $("input[name='institution_president']").val("");;
        $("input[name='institution_rep1_name']").val("");;
        $("input[name='institution_rep2_name']").val("");;
        $("input[name='institution_rep1_contact']").val("");;
        $("input[name='institution_rep2_contact']").val("");;
        $("input[name='corporate_companyname']").val("");;
        $("input[name='corporate_address']").val("");;
        $("input[name='corporate_rep']").val("");;
        $("input[name='corporate_rep_contact']").val("");;



    }else if(x == "institutional"){
        /*Show Elements*/
        $("#institution_affiliation").show("slow");
        $("#institution_address").show("slow");
        $("#institution_president").show("slow");
        $("#institution_rep1_name").show("slow");
        $("#institution_rep2_name").show("slow");
        $("#institution_rep1_contact").show("slow");
        $("#institution_rep2_contact").show("slow");

        /*Dynamic Adding of Required Attributes*/
        $("input[name='institution_affiliation']").prop('required', true);
        $("input[name='institution_address']").prop('required', true);
        $("input[name='institution_president']").prop('required', true);
        $("input[name='institution_rep1_name']").prop('required', true);
        $("input[name='institution_rep2_name']").prop('required', true);
        $("input[name='institution_rep1_contact']").prop('required', true);
        $("input[name='institution_rep2_contact']").prop('required', true);

        /*Dynamic Removing of Required Attributes*/
        $("input[name='individual_fname']").prop('required', false);
        $("input[name='individual_lname']").prop('required', false);
        $("input[name='individual_affiliation']").prop('required', false);
        $("input[name='individual_president']").prop('required', false);
        $("input[name='individual_attainment']").prop('required', false);
        $("input[name='corporate_companyname']").prop('required', false);
        $("input[name='corporate_address']").prop('required', false);
        $("input[name='corporate_rep']").prop('required', false);
        $("input[name='corporate_rep_contact']").prop('required', false);


        /*Hide Other Elements*/
        $('#individual_fname').hide("slow");
        $('#individual_lname').hide("slow");
        $('#individual_affiliation').hide("slow");
        $('#individual_president').hide("slow");
        $('#individual_attainment').hide("slow");
        $('#corporate_companyname').hide("slow");
        $('#corporate_address').hide("slow");
        $('#corporate_rep').hide("slow");
        $('#corporate_rep_contact').hide("slow");

        /*Clear Values*/
        $("input[name='individual_fname']").val("");;
        $("input[name='individual_lname']").val("");;
        $("input[name='individual_affiliation']").val("");;
        $("input[name='individual_president']").val("");;
        $("input[name='individual_attainment']").val("");;
        $("input[name='corporate_companyname']").val("");;
        $("input[name='corporate_address']").val("");;
        $("input[name='corporate_rep']").val("");;
        $("input[name='corporate_rep_contact']").val("");;
    }else if(x == "corporate"){
        /*Show Elements*/
        $('#corporate_companyname').show("slow");
        $('#corporate_address').show("slow");
        $('#corporate_rep').show("slow");
        $('#corporate_rep_contact').show("slow");

        /*Dynamic Adding of Required Attributes*/
        $("input[name='corporate_companyname']").prop('required', true);
        $("input[name='corporate_address']").prop('required', true);
        $("input[name='corporate_rep']").prop('required', true);
        $("input[name='corporate_rep_contact']").prop('required', true);

        /*Dynamic Removing of Required Attributes*/
        $("input[name='individual_fname']").prop('required', false);
        $("input[name='individual_lname']").prop('required', false);
        $("input[name='individual_affiliation']").prop('required', false);
        $("input[name='individual_president']").prop('required', false);
        $("input[name='individual_attainment']").prop('required', false);
        $("input[name='institution_affiliation']").prop('required', false);
        $("input[name='institution_address']").prop('required', false);
        $("input[name='institution_president']").prop('required', false);
        $("input[name='institution_rep1_name']").prop('required', false);
        $("input[name='institution_rep2_name']").prop('required', false);
        $("input[name='institution_rep1_contact']").prop('required', false);
        $("input[name='institution_rep2_contact']").prop('required', false);

        /*Hide Other Elements*/
        $("#institution_affiliation").hide("slow");
        $("#institution_address").hide("slow");
        $("#institution_president").hide("slow");
        $("#institution_rep1_name").hide("slow");
        $("#institution_rep2_name").hide("slow");
        $("#institution_rep1_contact").hide("slow");
        $("#institution_rep2_contact").hide("slow");
        $('#individual_fname').hide("slow");
        $('#individual_lname').hide("slow");
        $('#individual_affiliation').hide("slow");
        $('#individual_president').hide("slow");
        $('#individual_attainment').hide("slow");

        /*Clear Values*/
        $("input[name='individual_fname']").val("");;
        $("input[name='individual_lname']").val("");;
        $("input[name='individual_affiliation']").val("");;
        $("input[name='individual_president']").val("");;
        $("input[name='individual_attainment']").val("");;
        $("input[name='institution_affiliation']").val("");;
        $("input[name='institution_address']").val("");;
        $("input[name='institution_president']").val("");;
        $("input[name='institution_rep1_name']").val("");;
        $("input[name='institution_rep2_name']").val("");;
        $("input[name='institution_rep1_contact']").val("");;
        $("input[name='institution_rep2_contact']").val("");;
    }
    else{
        /*Hide All*/
        $("#institution_affiliation").hide("slow");
        $("#institution_address").hide("slow");
        $("#institution_president").hide("slow");
        $("#institution_rep1_name").hide("slow");
        $("#institution_rep2_name").hide("slow");
        $("#institution_rep1_contact").hide("slow");
        $("#institution_rep2_contact").hide("slow");
        $('#individual_fname').hide("slow");
        $('#individual_lname').hide("slow");
        $('#individual_affiliation').hide("slow");
        $('#individual_president').hide("slow");
        $('#individual_attainment').hide("slow");
        $('#corporate_companyname').hide("slow");
        $('#corporate_address').hide("slow");
        $('#corporate_rep').hide("slow");
        $('#corporate_rep_contact').hide("slow");

        /*Remove all Required*/
        $("input[name='individual_fname']").prop('required', false);
        $("input[name='individual_lname']").prop('required', false);
        $("input[name='individual_affiliation']").prop('required', false);
        $("input[name='individual_president']").prop('required', false);
        $("input[name='individual_attainment']").prop('required', false);
        $("input[name='institution_affiliation']").prop('required', false);
        $("input[name='institution_address']").prop('required', false);
        $("input[name='institution_president']").prop('required', false);
        $("input[name='institution_rep1_name']").prop('required', false);
        $("input[name='institution_rep2_name']").prop('required', false);
        $("input[name='institution_rep1_contact']").prop('required', false);
        $("input[name='institution_rep2_contact']").prop('required', false);
        $("input[name='corporate_companyname']").prop('required', false);
        $("input[name='corporate_address']").prop('required', false);
        $("input[name='corporate_rep']").prop('required', false);
        $("input[name='corporate_rep_contact']").prop('required', false);

        /*Clear All Values*/
        $("input[name='individual_fname']").val("");;
        $("input[name='individual_lname']").val("");;
        $("input[name='individual_affiliation']").val("");;
        $("input[name='individual_president']").val("");;
        $("input[name='individual_attainment']").val("");;
        $("input[name='institution_affiliation']").val("");;
        $("input[name='institution_address']").val("");;
        $("input[name='institution_president']").val("");;
        $("input[name='institution_rep1_name']").val("");;
        $("input[name='institution_rep2_name']").val("");;
        $("input[name='institution_rep1_contact']").val("");;
        $("input[name='institution_rep2_contact']").val("");;
        $("input[name='corporate_companyname']").val("");;
        $("input[name='corporate_address']").val("");;
        $("input[name='corporate_rep']").val("");;
        $("input[name='corporate_rep_contact']").val("");;

    }
});
</script>
@stop