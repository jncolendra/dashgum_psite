@extends('landingpage.loginlayout')
@section('content')
<br>
<br>
<br>
<form action="/submitPayment" class="form-login" style="max-width:45%" enctype="multipart/form-data" method="post">
	 <h2 class="form-login-heading">Membership: Approval and Payment</h2>
    	<div class="login-wrap">
    		<fieldset>
    			<img src="/profilephotos/{{Confide::user()->photofilename}}" height="50px" width="50px" align="left">
    			<span style="padding-left: 1em">Member Username: <b>{{Confide::user()->username}}</b></span><br>
    			<span style="padding-left: 1em">Membership Type: <b>{{Confide::user()->usertype}}</b></span><br>
    			<span style="padding-left: 1em">Status of Membership: <b>{{Confide::user()->approval}}</b></span>
    			<hr>
    			<span><h5>To have your membership verified by the administrators, you are encouraged to upload a picture of your verified deposit slip here.</h5></span>
    			<hr><br>
                @if(Confide::user()->payment_photo == NULL)
                @endif
    			<label class="col-lg-4"><b>Deposit Slip</b></label><input type="file" name="file" accept="image/*" capture="camera" id="file" require/>
				<hr>
                @if (Confide::user()->payment_photo)
                <h3>Your Payment Photo:</h3>
                <center><img src="/payment_photo/{{Confide::user()->payment_photo}}" class="img-responsive"><center>
                <span>You can reupload a photo again, if you've uploaded a wrong photo.</span>
                @endif
                @if(Confide::user()->payment_photo == NULL)
                @endif
    			<input id="btnsubmit" type="submit" class="btn btn-primary btn-block" disabled>
                <a href="/users/logout" class="btn btn-default btn-block">Log Out</a>
    		</fieldset>
		</div>
</form>
<script>
    $('#file').change(function(){
        $('#btnsubmit').prop('disabled',false);
    });
</script>
@stop