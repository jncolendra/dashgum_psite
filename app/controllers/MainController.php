<?php

class MainController extends BaseController {
	//Controller Functions Which will be used by the application.
	/*Admin Controllers - START*/
	public function createEvent(){ //still in development
		$event_title = Input::get('event_title');
		$event_date	 = Input::get('event_date');
		$event_place = Input::get('event_place');
		$event_description = Input::get('event_description');
		$get_id	= Confide::user()->id;
		

		$file = Input::file('file');

		/* Database Insertion */
		$destination = public_path().'/eventphotos';
		$extension = $file->getClientOriginalExtension();
		$file_string = date("YmdHis");
		$file_name = $file_string.".".$extension;
		$database = DB::table('events')->insert(
    		array(
    			'event_title' => $event_title,
    			'event_date' => $event_date,
    			'event_place' => $event_place,
    			'event_description' => $event_description,
    			'event_photo' => $file_name,
    			'event_creator_id' => $get_id,
    			'created_at' =>	date("Y-m-d")
    			)
		);
		$upload_success = $file->move($destination, $file_string.".".$extension);


		//Start Test
		$dataToIterate = DB::table('users')->lists("id");
		foreach ($dataToIterate as $key) {
			DB::table('notifications')->insert(
				array(
					"sender_id" => Confide::user()->id,
					"receiver_id" => $key,
					"type_id" => 1,
					"created_at" => date("Y-m-d h-m-s"),
					"extra" => $event_title,
					"state" => "1"
				)
			);
		}
		//endTest
		Session::put('notice',"Succesfully Created an Event!");
		return Redirect::to('/createannouncement');
	}

	public function readNotifs($id, $id2, $id3){
		$query = DB::table('notifications')->where('id',$id)->update(array(
			"state" => 0
			));
		if ($id3 == 1){
			return Redirect::to ('/events/'.$id2);

		}
		elseif ($id3 == 2){
			return Redirect::to ('/profile/'.$id2);

		}
	}
	public function deleteInactive($id){
		$delete = User::where('id',$id);
		$delete->delete();
		return Redirect::to('/ActiveMembers');
	}
	public function updateEvent(){
		DB::table('events')->where('id', Input::get('event_id'))->update(array(
			"event_title" => Input::get('event_title'),
			"event_date" => Input::get('event_date'),
			"event_description" => Input::get('event_description'),
		));
		Session::put('notice',"Succesfully Updated the Event!");
		return Redirect::to('/listofevents');
	}
	public function viewEvents($id){
		$events = DB::table('events')->where('id',$id)->first();
		return View::make('app.events')->with('events',$events);
	}

	public function listofEvents(){
		$events = DB::table('events')->get();
		return View::make('admin.listofEvents')->with('events', $events);
	}

	public function deleteEvent($id){
		$deleteEvent = EventsTable::find($id);
		$deleteEvent->delete();
		Session::put('notice',"Succesfully Deleted the Event!");
		return Redirect::to('/listofevents');
	}


	public function settlePayment(){
		$payment = Input::get('settlePayment');
		$id = Input::get('userID');
		DB::table('users')->where('id', $id)
            ->update(array('payment' => $payment));
        Session::flash('notice','Succesfully Updated Payment!');
        return Redirect::to('/accounting');
	}
	public function submitPayment(){
		$file = Input::file('file');
		$destination = public_path().'/payment_photo';
		$extension = $file->getClientOriginalExtension();
		$file_string = "payment".Confide::user()->username;
		$file_name = $file_string.".".$extension;
		DB::table('users')->where('id', Confide::user()->id)
            ->update(array('payment_photo' => $file_name));
        $upload_success = $file->move($destination, $file_string.".".$extension);
        return Redirect::to('/payment');
	}

	public function listofMembers_individual(){
		$users = User::where('usertype','individual')->get();
		return View::make('admin.listofMembers_individual')->with('users',$users);
	}
	public function listofMembers_institutional(){
		$users = User::where('usertype','institutional')->get();
		return View::make('admin.listofMembers_institutional')->with('users',$users);
	}
	public function listofMembers_corporate(){
		$users = User::where('usertype','corporate')->get();
		return View::make('admin.listofMembers_corporate')->with('users',$users);
	}
	public function profilePhoto(){
		$get_username = Confide::user()->username;
		$file = Input::file('file');
		$destination = public_path().'/profilephotos';
		$extension = $file->getClientOriginalExtension();
		$file_name = $get_username.".".$extension;
		User::where('username', $get_username)->update(array(
			"photofilename" => $file_name
		));
		$upload_success = $file->move($destination, $get_username.".".$extension);
		
		return Redirect::to('/dashboard');
	}
	
	public function getOnline(){
		$get_onlineusers = DB::table('users')->where('is_online',1)->get();
		return $get_onlineusers;
	}

	public function approveUser($id){
		
		/*Mails the User that he has been approved! - START*/
		$data = DB::table('users')->where('id',$id)->first();
		if($data->payment == 0 ){
			Session::put("unpaid","You have not yet settled the payment of: ".$data->username.", please consider settling his payments first.");
			return Redirect::to("/approvalofmembers");
		}else{
			Mail::queue('emails.approved', array("username" => $data->username), function($message) use ($data)
			{
			    $message->to($data->email)->subject('Account Approval');
			});
			/*@STOP*/
			
			/*Approves the User - Start*/
			$approve_user = User::where('id', $id)->update(array(
				"approval" => "approved" 
			));
			Session::put('notice',"You have successfully approved user:".$data->username);
			return Redirect::to("/approvalofmembers");
			/*@STOP*/
		}
	}

	public function getUsername(){
		$input = Input::get('input');
		$result = DB::table('users')->where('username','LIKE', '%'.$input.'%')->get();
		return $result;
	}

	public function searchBar(){
		$x = Input::get("searchboxinput");
		if($x !== ""){
		$data = DB::table("users")->where("username", "LIKE" , "%".$x."%")->get();
		$eventdata=DB::table("events")->where("event_title","LIKE","%".$x."%")->get();	
		}
		if(Request::Ajax()){			
			return array($data,$eventdata);
		}else{
			Session::put("query", $x);
			return Redirect::to("/searchResults");
		}
	}
		
	/*@STOP*/

	/*Creates A Conversation*/
	public function createConvo($receiverId){
		if(Request::ajax()){
			$conv_id = TBMsg::getConversationByTwoUsers(Confide::user()->id, $receiverId); // This Line Gets two user IDs and finds if they have existing conversation.
			$receiver = DB::table('users')->where('id', $receiverId)->first();
			$messages = TBMsg::getConversationMessages($conv_id, Confide::user()->id);
			TBMsg::markReadAllMessagesInConversation($conv_id, Confide::user()->id);
			if($conv_id == -1){
				$createChat = TBMsg::createConversation(array(Confide::user()->id, $receiverId));
				return View::make('app.realtimeMessaging')->with('receiver',  $receiver)->with('messages',$messages->getAllMessages());
			}else{
				return View::make('app.realtimeMessaging')->with('receiver',  $receiver)->with('messages',$messages->getAllMessages());
			}
		}else{
			$conv_id = TBMsg::getConversationByTwoUsers(Confide::user()->id, $receiverId); // This Line Gets two user IDs and finds if they have existing conversation.
			$receiver = DB::table('users')->where('id', $receiverId)->first();
			$messages = TBMsg::getConversationMessages($conv_id, Confide::user()->id);
			TBMsg::markReadAllMessagesInConversation($conv_id, Confide::user()->id);
			if($conv_id == -1){
				$createChat = TBMsg::createConversation(array(Confide::user()->id, $receiverId));
				return View::make('app.message')->with('receiver',  $receiver)->with('messages',$messages->getAllMessages());
			}else{

				return View::make('app.message')->with('receiver',  $receiver)->with('messages',$messages->getAllMessages());
			}	
		}
	}

	/*Sends A Message to a Conversation*/
	public function sendMessage(){
		$content = Input::get("message");
		$receiverId = Input::get("receiverId");
		$conv_id = TBMsg::getConversationByTwoUsers(Confide::user()->id, $receiverId);
		$conv = TBMsg::addMessageToConversation($conv_id, Confide::user()->id, $content);
		return Redirect::to('/sendMessage/'.$receiverId);
	}
	public function getProfileData($username){
		$profileData = User::where("username", $username)->first();
		return View::make('app.profile')->with('profileData', $profileData); 
	}
	public function viewInbox(){
		$userconvos = TBMsg::getUserConversations(Confide::user()->id);

		return View::make('app.inbox')->with('userconvos', $userconvos);
	}
	public function submitAttendance(){
		$event_id 	= Input::get("event_id");
		$attendees 	= Input::get("attendees");
		foreach ($attendees as $attendee){
			DB::table('event_attendance')->insert(
				array(
					"event_id" => $event_id,
					"user_id"  => $attendee
				)
			);
		}
		Session::put('notice',"Succesfully Updated the Event!");
		return Redirect::to('/eventAttendance');
	}
	public function deleteAttendance($id, $id2){
		DB::table('event_attendance')->where("event_id", $id)->where("user_id",$id2)->delete();
		Session::put('notice',"Succesfully Deleted the Record!");
		return Redirect::to('/eventListAttendance');
	}

	public function ajaxIdle(){
		
			$get_username = Confide::user()->username;
                $update = User::where('username',$get_username)->update(array(
                    'is_online' => 0
        	));
		
	}
	public function ajaxActive(){
		
			$get_username = Confide::user()->username;
	                $update = User::where('username',$get_username)->update(array(
	                    'is_online' => 1
	        ));
		
	}
	public function getMsgCount(){
			return TBMsg::getNumOfUnreadMsgs(Confide::user()->id);
	}
	public function realtimeQuickInbox(){
		return View::make("app.realtimeQuickbox");
	}
	public function realtimeNotif(){
		return View::make("app.realtimeNotifs");
	}
	public function getNotifCount(){
		return DB::table('notifications')->where('receiver_id',Confide::user()->id)
		->where('state', 1)
		->count();
	}
	public function realtimeInbox(){
		return View::make("app.realtimeInbox");
	}
	public function submitRenewal(){
		$id = Input::get("user_id");
		$conditional = DB::table('users')->where('id',$id)->pluck('first_renewal_date');
		$conditional2 = DB::table('renewals')->where('user_id',$id)->orderBy('renewal_date', 'desc')->pluck('renewal_date');
		$if_exist_renewal = DB::table('renewals')->where('user_id',$id)->first();
		$query = DB::table('users')->where('id',$id)->first();
		if($query->usertype == "individual"){
			$payment = 500.00;
		}
		elseif($query->usertype == "institutional")
		{
			$payment = 3000.00;	
		}
		elseif($query->usertype == "corporate")
		{
			$payment = 10000.00;
		}
		/*Real Condition*/
		if($payment == 0){
			Session::put("notice", "Must be a non-zero value!");
			return Redirect::to("/payRen");
		}
		if ($if_exist_renewal == NULL) {
			//Run This statement if user has not record of renewal, retrieve values of user db
			DB::table('renewals')->insert(
				array(
					"user_id" => $id,
					"payment_amount" => $payment,
					"renewal_date" => date("Y-m-d",strtotime($conditional."-1 days +1 years"))
				)
			);
			Session::put('notice',"Renewal of Transaction Succesful");
			return Redirect::to('/payRen');
		}else{
			//if user has already a prior renewal, get the date from this database and operate
			if ($conditional2) {
			DB::table('renewals')->insert(
				array(
					"user_id" => $id,
					"payment_amount" => $payment,
					"renewal_date" => date("Y-m-d",strtotime($conditional2."-1 days +1 years"))
				)
			);
			Session::put('notice',"Renewal of Transaction Succesful");
			return Redirect::to('/payRen');	
			}
		}
	}
}
